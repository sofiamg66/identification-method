
names2 = {'Wrist rotation','Arm rotation','Ankle rotation','Leg flexion/extension','Roll side to side'};
num = num2str(9);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
s = s2;

x1 = ['wristrotation',s,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotation',s,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotation',s,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s,'_',num,num2];
x_5 = eval(x5);

x1m = ['wristrotation',s,'_',num,num2,'_2'];
x_1m = eval(x1m);
x2m = ['armrotation',s,'_',num,num2,'_2'];
x_2m = eval(x2m);
x3m = ['anklerotation',s,'_',num,num2,'_2'];
x_3m = eval(x3m);
x4m = ['legmove',s,'_',num,num2,'_2'];
x_4m = eval(x4m);
x5m = ['random',s,'_',num,num2,'_2'];
x_5m = eval(x5m);

x_1s = [x_1;x_1m];
x_2s = [x_2;x_2m];
x_3s = [x_3;x_3m];
x_4s = [x_4;x_4m];
x_5s = [x_5;x_5m];

x = [x_1s x_2s x_3s x_4s x_5s];

box_x = x(31:40,:);
supine_box = x(21:30,4);
figure(1)
 boxplot([supine_box,x_1s(31:40,1),x_2s(31:40,1),x_3s(31:40,1),x_4s(31:40,1)],'Labels',{['Supine'],['Wrist'],['Arm'],['Ankle'],['Leg']});
ylabel('Impedance Value (ohms)','FontSize',16)
xtickangle(45)
hold on
title(['Box plot with arm segment bio-impedance at 90kHz'],'FontSize',16)
%title([sprintf('%s',names{i})]) 
%arreglar savefig, si no manually
%saveas(gcf,sprintf('boxplot_WB_%s.png',names{i}))
savefig(['boxplot_leg_',freq,'0.fig'])
