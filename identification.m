% IDENTIFICATION METHOD 
% Also, caculation of: mean for supine and motion artefact; percentage
% change in mean between supine and motion artefact; standard
% deviation for supine and motion artefact; percentage change in 
% standard deviation between supine and motion artefact

name1 = 'Wrist rotation';
name2 = 'Arm rotation';
name3 = 'Ankle rotation';
name4 = 'Leg movement';
name5= 'Roll side to side';
names = {name1;name2;name3;name4;name5};

std_supine = []; 
std_movement = [];
change_means = [];
change_std = [];
mean_supine = [];
mean_movement = [];
minval_std = [];
minval_mean = [];

max_sensitivity = [];
max_specificity = [];
max_accuracy = [];
min_sensitivity = [];
min_specificity= [];
min_accuracy = [];

artefact = [];
artefact_2 = [];
artefact_3 = [];
sensitivity =[];
specificity = [];
accuracy = [];
% choose the segment by changing in line 129 and 179 which segment, and

% threshold values for percentage change
WB = [0.03 0.09 0.34 0.01 0.04 0.1 0.85 0.25 0.45 0.19;3.97 98.95 16.3 502.92 21.40 145.62 21.08 419.44 45.2 101.24];
% threshold values for mean and std when using windowing method
WB_val = [2566.65, 1588.17, 355.87,1329.21,1314.80,1243.79,760.57,913.72,760.40,596.53;0.27,0.01,0.07, 0.01,0.16,0.39,0.36,0.13,0.21,0.05];

for freq=1:10
num = num2str(freq);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
s = s1;

std_s_supine = [];

mean_s_supine = [];
supine = [];
movement = [];
std_sum_move = [];
std_sum_sup = []; 
mean_sum = [0];
mean_summ = [0,0,0,0,0];

if freq ==10
num = num2str(1);
x1 = ['wristrotation',s,'_',num,num2,num2];
x_1 = eval(x1);
x2 = ['armrotation',s,'_',num,num2,num2];
x_2 = eval(x2);
x3 = ['anklerotation',s,'_',num,num2,num2];
x_3 = eval(x3);
x4 = ['legmove',s,'_',num,num2,num2];
x_4 = eval(x4);
x5 = ['random',s,'_',num,num2,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

% To use with the data from subject 2, since the data was recorded
% separately, you need to comment the x variable above

%x1m = ['wristrotation',s,'_',num,num2,'_2'];
%x_1m = eval(x1m);
%x2m = ['armrotation',s,'_',num,num2,'_2'];
%x_2m = eval(x2m);
%x3m = ['anklerotation',s,'_',num,num2,'_2'];
%x_3m = eval(x3m);
%x4m = ['legmove',s,'_',num,num2,'_2'];
%x_4m = eval(x4m);
%x5m = ['random',s,'_',num,num2,'_2'];
%x_5m = eval(x5m);

%x_1s = [x_1;x_1m];
%x_2s = [x_2;x_2m];
%x_3s = [x_3;x_3m];
%x_4s = [x_4;x_4m];
%x_5s = [x_5;x_5m];
%x = [x_1s x_2s x_3s x_4s x_5s];
else 
x1 = ['wristrotation',s,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotation',s,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotation',s,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

% To use with the data from subject 2, since the data was recorded
% separately, you need to comment the x variable above
%x1m = ['wristrotation',s,'_',num,num2,'_2'];
%x_1m = eval(x1m);
%x2m = ['armrotation',s,'_',num,num2,'_2'];
%x_2m = eval(x2m);
%x3m = ['anklerotation',s,'_',num,num2,'_2'];
%x_3m = eval(x3m);
%x4m = ['legmove',s,'_',num,num2,'_2'];
%x_4m = eval(x4m);
%x5m = ['random',s,'_',num,num2,'_2'];
%x_5m = eval(x5m);

%x_1s = [x_1;x_1m];
%x_2s = [x_2;x_2m];
%x_3s = [x_3;x_3m];
%x_4s = [x_4;x_4m];
%x_5s = [x_5;x_5m];
%x = [x_1s x_2s x_3s x_4s x_5s];
end

% Iteration through the five movements studied; check the x variable to
% know the movments order
for i = 1:5
    i_m = 0;
        
    std_s = [];
mean_s = [];
combination_mean = [];
combination_std = [];
movement = [];
supine = [];

%Select the length of data to analyse; either 30 data points or 10.
movement(:,1) = x(31:40,i);
std_sum_move(:,i) = x(31:40,i);
supine(:,1) = x(21:30,i);
std_sum_sup(:,1) = supine(:,1);
length_supine = length(supine(:,1));

%Calculation of the mean and standard deviation avoiding the zeros from the
%ohms conversion code
i_s = 0;
for k = 1:length_supine
    if supine(k,1) ~= 0
        mean_sum(freq,1) = mean_sum(1,1)+supine(k,1);
        i_s = i_s+1;
    else
        disp('There is a zero in supine in ')
        k
        std_sum_sup(k,1) = NaN;
    end
end
mean_supine(freq,1) = mean_sum(freq,1)/i_s;
std_supine(freq,1) = std(std_sum_sup(:,1),'omitnan');

% Improvement for the code could be to only use a loop since std_sum_sup 
% and supine variables are the same

%Calculation of the windowing, avoiding the zeros from the ohms conversion code
supine(supine(:,1) ==0) = [];
for k = 1:(length(supine)-1)
        std_s_supine(k,1) = std(supine(k:k+1,1));        
        mean_s_supine(k,1) = mean(supine(k:k+1,1));
end
% calculation of the minimum threshold for each subject. This part was not
% used for the report, but it is an enhancement of the code
for k=1:(length(std_s_supine(:,1))-1)
diff_thres_std(:,1) = abs(std_s_supine(k+1,1)-std_s_supine(k,1))/std_s_supine(k,1);
diff_thres_mean(:,1) = abs(mean_s_supine(k+1,1)-mean_s_supine(k,1))/mean_s_supine(k,1);
end
minval_std(1,freq) = min(diff_thres_std(:,1));
minval_mean(1,freq) = min(diff_thres_mean(:,1));

length_movement = length(movement(:,1));
% calculating mean movement without considering the zeros from the
% ohms conversion code
for k = 1:length_movement
    if movement(k,1) ~= 0
        mean_summ(freq,i) = mean_summ(1,i)+movement(k,1);
        i_m = i_m+1; 
    else
        disp('There is a zero in movement in ')
        k
        std_sum_move(k,i) = NaN;
    end
end
mean_movement(freq,i) = mean_summ(freq,i)/i_m;

% calculating standard deviation for movement
std_movement(freq,i) = std(std_sum_move(:,i),'omitnan');

%% Identification method 1
% calculate percentage change in mean and std between supine and movement 
change_means(freq,i) = abs(mean_movement(freq,i)-mean_supine(freq,1))/mean_supine(freq,1);
change_std(freq,i) = abs(std_movement(freq,i)-std_supine(freq,1))/std_supine(freq,1);

% identification factor taking the whole mean and std from reference, 30
% data points; WB is the vector with the selected thresholds
if change_means(freq,i) ==0
elseif change_std(freq,i) ==0
    disp('error in ',freq,' ',i)
else
    if change_std(freq,i)>WB(2,freq)%(change_means(freq,i) > WB(1,freq)) && (change_std(freq,i) > WB(2,freq))
        artefact(freq,i) = 1;
        disp('noise')
    else
        artefact(freq,i) = 0;
 end
end
% windowing every two points, calculating the mean
% improvement could be to use only one loop since std_sum_move is the same
% as movement
%% Identification method 2 with two points windowing
movement(movement(:,1) ==0) = [];
for k = 1:(length(movement(:,1))-1)
    std_s(k,1) = std(movement(k:k+1,1));
    mean_s(k,1) = mean(movement(k:k+1,1));
end

% combine supine and movement in one vector to run the identification
% method, avoiding the zeros from the ohms conversion code
mean_s_supine(mean_s_supine(:,1)==0) = [];
length_mean_s_supine = length(mean_s_supine(:,1));
mean_s(mean_s(:,1)==0) = [];
length_mean_s = length(mean_s);
combination_mean(:,1) = [mean_s_supine(:,1);mean_s(:,1)];
combination_std(:,1) = [std_s_supine(:,1);std_s(:,1)];
combination_std(combination_std(:,1)==0) = [];

% select the length of the identification method. It is extensively done
% because of the problem with the zeros. It was to assure a correct length
% of the identification method
length_combination(1,i) = length(combination_mean(:,1));
length_combinationstd(1,i) = length(combination_std(:,1));
if length_combination(1,i) > length_combinationstd(1,i)
length_loop = length_combinationstd(1,i);
else
    length_loop = length_combination(1,i);
end
for j = 1:(length_loop-1)
    
% identification factor taking two data points
    if combination_mean(j,1) == 0
    elseif combination_mean(j+1,1) == 0
    else 
        diff_mean(j,i,freq) = abs(combination_mean(j+1,1)-combination_mean(j,1))/combination_mean(j,1);
    end
    if combination_std(j,1) == 0 
    elseif combination_std(j+1,1) == 0
        
    else
        diff_std(j,i,freq) = abs(combination_std(j+1,1)-combination_std(j,1))/combination_std(j,1);
    end
    %identification of percentage change windowing with threshold
    %percentage change (2 data points). I believe this method should not be
    %used but rather the method below
    if diff_std(j,i,freq)>WB_val(2,freq)
      artefact_2(j,i,freq) = 1;
   elseif diff_mean(j,i,freq)>WB_val(1,freq)
        artefact_2(j,i,freq) = 1;
    else
        artefact_2(j,i,freq) = 0;
    end
    %identification of percentage change windowing with threshold
    %percentage change windowing from data set 1. I believe this is the
    %best method to use with the identification algorithm
    if diff_std(j,i,freq)>minval_std(1,freq)
                artefact_3(j,i,freq) = 1;
    elseif diff_mean(j,i,freq)>minval_mean(1,freq)
        artefact_3(j,i,freq) = 1;
    else
        artefact_3(j,i,freq) = 0;
    end
end
% variables to plot the discovery
    finalpoint= length(combination_mean(:,1))*1.2;
    length_art2 = length(combination_mean(:,1));
    datapoints = linspace(0,finalpoint,length_art2);

% Plot the discovery from artefact_3
    %plotdiscovery(combination_mean(:,1),artefact_3(:,i,freq),length_mean_s_supine,datapoints',names(i),i,freq)

end


separator = zeros(1,5);
% automatically save in an excel the result from method 1 taking the 30
% measurement data points
data1 = [artefact(freq,:)];
data_1 = array2table(data1);
data_1.Properties.VariableNames = {'wristrotate' 'armrotate' 'anklerotate' 'legmovement' 'rollsidetoside'};
if freq ==10
%writetable(data_1,['artefacts_',s,'_freq',num,'00.xls']);
else  
%writetable(data_1,['artefacts_',s,'_freq',num,'0.xls']);
end
end

% automatically save in an excel the minimum thresholds for each frequency
% for the subject
data3 = [minval_std(1,:);minval_mean(1,:)];
data_3 = array2table(data3);
%data_3.Properties.VariableNames = {'10kHz' '20kHz' '30kHz' '40kHz' '50kHz' '60kHz' '70kHz' '80kHz' '90kHz'};
%writetable(data_3,['newminvals_',s,'.xls']);

%% Calculate accuracy, specificity, sensitivity for artefact_2
falsepos = zeros(10,5);
falseneg = zeros(10,5);
trueneg = [];
truepos = [];
for freq = 1:10
for i = 1:5
    length_artefact = length(artefact_2(:,i));
for k = 1:length_mean_s_supine
    if artefact_2(k,i,freq) == 1
       falsepos(freq,i) = 1+falsepos(freq,i);
    end
end
trueneg(freq,i) = length_mean_s_supine-falsepos(freq,i);
lengthpos = length_artefact-length_mean_s_supine;
for k = length_mean_s_supine:length_artefact
    if artefact_2(k,i,freq) == 0 
        falseneg(freq,i) = 1+falseneg(freq,i);
    end
end
truepos(freq,i) = lengthpos-falseneg(freq,i);


sum_total(freq,i) = trueneg(freq,i)+falseneg(freq,i)+truepos(freq,i)+falsepos(freq,i);
%literature 
    sensitivity(freq,i) = truepos(freq,i)/(truepos(freq,i)+falseneg(freq,i))*100;          %  = truepos+falseneg
    specificity(freq,i) = trueneg(freq,i)/(trueneg(freq,i)+falsepos(freq,i))*100;           %  = trueneg+falsepos
    accuracy(freq,i) = (truepos(freq,i)+trueneg(freq,i))/(sum_total(freq,i))*100; % 38 = truepos+trueneg+falsepos+falseneg
end
end
%% Calculate accuracy, specificity, sensitivity for artefact_3
falsepos = zeros(10,5);
falseneg = zeros(10,5);
trueneg = [];
truepos = [];
sum_total = [];
for freq = 1:10
for i = 1:5
    length_artefact = length(artefact_3(:,i));
for k = 1:length_mean_s_supine
    if artefact_3(k,i,freq) == 1
       falsepos(freq,i) = 1+falsepos(freq,i);
    end
end
trueneg(freq,i) = length_mean_s_supine-falsepos(freq,i);
lengthpos = length_artefact-length_mean_s_supine;
for k = length_mean_s_supine:length_artefact
    if artefact_3(k,i,freq) == 0 
        falseneg(freq,i) = 1+falseneg(freq,i);
    end
end
truepos(freq,i) = lengthpos-falseneg(freq,i);


sum_total(freq,i) = trueneg(freq,i)+falseneg(freq,i)+truepos(freq,i)+falsepos(freq,i);
%literature 
    sensitivity_3(freq,i) = truepos(freq,i)/(truepos(freq,i)+falseneg(freq,i))*100;          %  = truepos+falseneg
    specificity_3(freq,i) = trueneg(freq,i)/(trueneg(freq,i)+falsepos(freq,i))*100;           %  = trueneg+falsepos
    accuracy_3(freq,i) = (truepos(freq,i)+trueneg(freq,i))/(sum_total(freq,i))*100; % 38 = truepos+trueneg+falsepos+falseneg
end
max_sensitivity(freq,1) = max(sensitivity_3(freq,:));
max_specificity(freq,1) = max(specificity_3(freq,:));
max_accuracy(freq,1) = max(accuracy_3(freq,:));
min_sensitivity(freq,1) = min(sensitivity_3(freq,:));
min_specificity(freq,1) = min(specificity_3(freq,:));
min_accuracy(freq,1) = min(accuracy_3(freq,:));
end

% Automatically save the metrics
separator = zeros(1,5);
dataprint = [sensitivity;separator;specificity;separator;accuracy];
data_temp = array2table(dataprint);
%data.Properties.VariableNames = {'wristrotation','armrotation','anklerotation','legmove','random'}
%writetable(data,['freq',num,'0_',s,'_factors.xls']);

% Atomatically save the minimum and maximum from the metrics
separator_2 = ones(10,1);
separator_3 = ones(1,8);
%dataprint_2 = [max_sensitivity(:,1),min_sensitivity(:,1),separator_2,max_specificity(:,1),min_sensitivity(:,1),separator_2,max_accuracy(:,1),min_accuracy(:,1)];
dataprint_2 = [max_sensitivity(:,1),min_sensitivity(:,1),separator_2,max_specificity(:,1),min_specificity(:,1),separator_2,max_accuracy(:,1),min_accuracy(:,1)];
data_temp_2 = array2table(dataprint_2);
%data_temp_2.Properties.VariableNames = {'max sensitivity','min sensitivity',' ','max specificity','min specificity',' ','max accuracy','min accuracy'}
%writetable(data_temp_2,[s,'_factors.xls']);
%clear all

function plotdiscovery(mean_plot,artefact_plot,length_supine,pointer,names,i,freq)
% plot of the mean points when windowing and the artefact detection points
% red: motion artefact detection, black: no motion artefact detection
figure(1)
plot(pointer(1:length_supine,1),mean_plot(1:length_supine,1),'b')
hold on 
plot(pointer(length_supine+1:end,1),mean_plot(length_supine+1:end,1),'r')
for p = 1:length(artefact_plot)
    if artefact_plot(p) == 1
        plot(pointer(p),mean_plot(p),'xr','HandleVisibility','off')
    else
        plot(pointer(p),mean_plot(p),'ok','HandleVisibility','off')
    end
end
xlabel('Time (s)', 'FontSize',10)
ylabel('Impedance Value (ohms)','FontSize',10)
n_legend = cell2mat(names);
legend({'Supine',n_legend},'FontSize',14,'Location','Northeast')
%figname = 
figname = (['Whole body segment ',names,' artefact detection at 10kHz']);
title(figname,'FontSize',14)
filename =sprintf('%d_windowing%d0Hz', i, freq);
savefig(filename)
close all
end