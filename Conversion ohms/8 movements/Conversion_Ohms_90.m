%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(9);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;
%%
x1 = ['wristrotation',s1,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s1,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s1,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s1,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s1,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s1,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s1,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s1,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

    for m = 1:8
Datafile = x(:,1+j*3:3+j*3);


for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 90 kHz

for i = 1:1:40
%         for i =25:1:27
 %***************************************************************************        
    for s = 1:1:11                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_90K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_90K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,4)-Avg_Freq_90K(Index+1,4))); % calculate slope for interpolation
                    b_90_1(i)=  ImpedanceSequence(Index)- (a_90_1(i)*Avg_Freq_90K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_1(i) =0; b_90_1(i)=0;    
              end
    end
         measurement_ohm_90K_1(i,1) = (a_90_1(i)*Datafile(i,4)) + b_90_1(i); 
   
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:11                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_90K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))); % calculate slope for interpolation
                    b_i_90_1(i)=  ImpedanceSequence(Index)- (a_i_90_1(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_1(i) =0; b_i_90_1(i)=0;    
              end
            
   end
   img_90K_1(i,1) = (a_i_90_1(i)*Datafile(i,3)) + b_i_90_1(i);  



    if (abs(img_90K_1(i,1)-measurement_ohm_90K_1(i,1))> 0)
        if(abs(img_90K_1(i,1)- measurement_ohm_90K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_90K_1(i,1);
        end
        
    end 
% end
 %***************************************************************************     
%  for i = 81:1:90 
 for s = 12:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_90K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_90K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,4)-Avg_Freq_90K(Index+1,4))); % calculate slope for interpolation
                    b_90_2(i)=  ImpedanceSequence(Index)- (a_90_2(i)*Avg_Freq_90K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_2(i) =0; b_90_2(i)=0;    
              end
    end
            measurement_ohm_90K_2(i,1) = (a_90_2(i)*Datafile(i,4)) + b_90_2(i);
 
 % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 12:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_90K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))); % calculate slope for interpolation
                    b_i_90_2(i)=  ImpedanceSequence(Index)- (a_i_90_2(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_2(i) =0; b_i_90_2(i)=0;    
              end
            
   end
   img_90K_2(i,1) = (a_i_90_2(i)*Datafile(i,3)) + b_i_90_2(i); 



    if (abs(img_90K_2(i,1)-measurement_ohm_90K_2(i,1))> 0)
        
        if(abs(img_90K_2(i,1)- measurement_ohm_90K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_90K_2(i,1);
        end
        
    end  
end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationWB_90 = ohms(:,1);
wristupdownWB_90 = ohms(:,2);
armrotationWB_90 = ohms(:,3);
armupdownWB_90 = ohms(:,4);
anklerotationWB_90 = ohms(:,5);
ankleupdownWB_90 = ohms(:,6);
legmoveWB_90 = ohms(:,7);
randomWB_90 = ohms(:,8);

save('wristrotationWB_90.mat','wristrotationWB_90');
save('wristupdownWB_90','wristupdownWB_90');
save('armrotationWB_90','armrotationWB_90');
save('armupdownWB_90','armupdownWB_90');
save('anklerotationWB_90','anklerotationWB_90');
save('ankleupdownWB_90','ankleupdownWB_90');
save('legmoveWB_90','legmoveWB_90');
save('randomWB_90','randomWB_90');

x1 = ['wristrotation',s2,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s2,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s2,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s2,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s2,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s2,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s2,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s2,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j=0
    for m = 1:8
Datafile = x(:,1+j*3:3+j*3);


for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 90 kHz

for i = 1:1:40
%         for i =25:1:27
 %***************************************************************************        
    for s = 1:1:11                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_90K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_90K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,4)-Avg_Freq_90K(Index+1,4))); % calculate slope for interpolation
                    b_90_1(i)=  ImpedanceSequence(Index)- (a_90_1(i)*Avg_Freq_90K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_1(i) =0; b_90_1(i)=0;    
              end
    end
         measurement_ohm_90K_1(i,1) = (a_90_1(i)*Datafile(i,4)) + b_90_1(i); 
   
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:11                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_90K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))); % calculate slope for interpolation
                    b_i_90_1(i)=  ImpedanceSequence(Index)- (a_i_90_1(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_1(i) =0; b_i_90_1(i)=0;    
              end
            
   end
   img_90K_1(i,1) = (a_i_90_1(i)*Datafile(i,3)) + b_i_90_1(i);  



    if (abs(img_90K_1(i,1)-measurement_ohm_90K_1(i,1))> 0)
        if(abs(img_90K_1(i,1)- measurement_ohm_90K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_90K_1(i,1);
        end
        
    end 
% end
 %***************************************************************************     
%  for i = 81:1:90 
 for s = 12:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_90K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_90K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,4)-Avg_Freq_90K(Index+1,4))); % calculate slope for interpolation
                    b_90_2(i)=  ImpedanceSequence(Index)- (a_90_2(i)*Avg_Freq_90K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_2(i) =0; b_90_2(i)=0;    
              end
    end
            measurement_ohm_90K_2(i,1) = (a_90_2(i)*Datafile(i,4)) + b_90_2(i);
 
 % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 12:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_90K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))); % calculate slope for interpolation
                    b_i_90_2(i)=  ImpedanceSequence(Index)- (a_i_90_2(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_2(i) =0; b_i_90_2(i)=0;    
              end
            
   end
   img_90K_2(i,1) = (a_i_90_2(i)*Datafile(i,3)) + b_i_90_2(i); 



    if (abs(img_90K_2(i,1)-measurement_ohm_90K_2(i,1))> 0)
        
        if(abs(img_90K_2(i,1)- measurement_ohm_90K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_90K_2(i,1);
        end
        
    end  
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationArm_90 = ohms(:,1);
wristupdownArm_90 = ohms(:,2);
armrotationArm_90 = ohms(:,3);
armupdownArm_90 = ohms(:,4);
anklerotationArm_90 = ohms(:,5);
ankleupdownArm_90 = ohms(:,6);
legmoveArm_90 = ohms(:,7);
randomArm_90 = ohms(:,8);


save('wristrotationArm_90','wristrotationArm_90');
save('wristupdownArm_90','wristupdownArm_90');
save('armrotationArm_90','armrotationArm_90');
save('armupdownArm_90','armupdownArm_90');
save('anklerotationArm_90','anklerotationArm_90');
save('ankleupdownArm_90','ankleupdownArm_90');
save('legmoveArm_90','legmoveArm_90');
save('randomArm_90','randomArm_90');

%%
x1 = ['wristrotation',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s3,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s3,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s3,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s3,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j = 0
    for m = 1:8
Datafile = x(:,1+j*3:3+j*3);


for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 90 kHz

for i = 1:1:40
%         for i =25:1:27
 %***************************************************************************        
    for s = 1:1:11                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_90K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_90K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,4)-Avg_Freq_90K(Index+1,4))); % calculate slope for interpolation
                    b_90_1(i)=  ImpedanceSequence(Index)- (a_90_1(i)*Avg_Freq_90K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_1(i) =0; b_90_1(i)=0;    
              end
    end
         measurement_ohm_90K_1(i,1) = (a_90_1(i)*Datafile(i,4)) + b_90_1(i); 
   
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:11                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_90K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))); % calculate slope for interpolation
                    b_i_90_1(i)=  ImpedanceSequence(Index)- (a_i_90_1(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_1(i) =0; b_i_90_1(i)=0;    
              end
            
   end
   img_90K_1(i,1) = (a_i_90_1(i)*Datafile(i,3)) + b_i_90_1(i);  



    if (abs(img_90K_1(i,1)-measurement_ohm_90K_1(i,1))> 0)
        if(abs(img_90K_1(i,1)- measurement_ohm_90K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_90K_1(i,1);
        end
        
    end 
% end
 %***************************************************************************     
%  for i = 81:1:90 
 for s = 12:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_90K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_90K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,4)-Avg_Freq_90K(Index+1,4))); % calculate slope for interpolation
                    b_90_2(i)=  ImpedanceSequence(Index)- (a_90_2(i)*Avg_Freq_90K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_2(i) =0; b_90_2(i)=0;    
              end
    end
            measurement_ohm_90K_2(i,1) = (a_90_2(i)*Datafile(i,4)) + b_90_2(i);
 
 % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 12:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_90K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))); % calculate slope for interpolation
                    b_i_90_2(i)=  ImpedanceSequence(Index)- (a_i_90_2(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_2(i) =0; b_i_90_2(i)=0;    
              end
            
   end
   img_90K_2(i,1) = (a_i_90_2(i)*Datafile(i,3)) + b_i_90_2(i); 



    if (abs(img_90K_2(i,1)-measurement_ohm_90K_2(i,1))> 0)
        
        if(abs(img_90K_2(i,1)- measurement_ohm_90K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_90K_2(i,1);
        end
        
    end  
end
ohms(:,m) = measured_ohms;
j = j+1;
    end
wristrotationLeg_90 = ohms(:,1);
wristupdownLeg_90 = ohms(:,2);
armrotationLeg_90 = ohms(:,3);
armupdownLeg_90 = ohms(:,4);
anklerotationLeg_90 = ohms(:,5);
ankleupdownLeg_90 = ohms(:,6);
legmoveLeg_90 = ohms(:,7);
randomLeg_90 = ohms(:,8);
    
save('wristrotationLeg_90','wristrotationLeg_90');
save('wristupdownLeg_90','wristupdownLeg_90');
save('armrotationLeg_90','armrotationLeg_90');
save('armupdownLeg_90','armupdownLeg_90');
save('anklerotationLeg_90','anklerotationLeg_90');
save('ankleupdownLeg_90','ankleupdownLeg_90')
save('legmoveLeg_90','legmoveLeg_90')
save('randomLeg_90','randomLeg_90')