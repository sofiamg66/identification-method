%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(1);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;
%%
x1 = ['wristrotation',s1,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s1,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s1,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s1,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s1,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s1,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s1,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s1,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

%% 10kHz
 for i = 1:1:40                                                               % Go through all data points for 10kHZ
%*************************************************************************%        
% Calculate if data poit is before 400 ohm 
for s = 1:1:4                                                                 % impedance inflection at 400 ohm
        
              Condition1 = gt(Datafile(i,4),Avg_Freq_10K(s,4))  ;              % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_10K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_10_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,4)-Avg_Freq_10K(Index+1,4))); % calculate slope for interpolation
                    b_10_1(i)=  ImpedanceSequence(Index)- (a_10_1(i)*Avg_Freq_10K(Index,4));                                              % calculate intercept for interpolation
                    
                    break;                                                         % store the index
              else a_10_1(i) =0; b_10_1(i)=0; 
                 
              end
              
end
 measurement_ohm_10K_1(i,1) = (a_10_1(i)*Datafile(i,4)) + b_10_1(i);
 
% Check the real values to check if above point is before the 400 ohm infection        
   for s = 1:1:4                                                         
        
              Condition1= le(Datafile(i,2),Avg_Freq_10K(s,2)) ;           % Comparing real code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,2),Avg_Freq_10K(s+1,2));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_r_10_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,2)-Avg_Freq_10K(Index+1,2))) ;% calculate slope for interpolation
                    b_r_10_1(i)=  ImpedanceSequence(Index)- (a_r_10_1(i)*Avg_Freq_10K(Index,2));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_r_10_1(i) =0; b_r_10_1(i)=0;    
              end
            
   end
   real_10K_1(i,1) = (a_r_10_1(i)*Datafile(i,2)) + b_r_10_1(i) ; 
   
   if (real_10K_1(i,1)-measurement_ohm_10K_1(i,1)> 0)
        
        if(real_10K_1(i,1)- measurement_ohm_10K_1(i,1)<= 600)
        measured_ohms(i,1)= measurement_ohm_10K_1(i,1);
        end
        
    end

%*************************************************************************%     

for s = 5:1:29                                                        % impedance inflection after 400 ohm
        
              Condition1= le(Datafile(i,4),Avg_Freq_10K(s,4))    ;              % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_10K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_10_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,4)-Avg_Freq_10K(Index+1,4))); % calculate slope for interpolation
                    b_10_2(i)=  ImpedanceSequence(Index)- (a_10_2(i)*Avg_Freq_10K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_10_2(i) =0; b_10_2(i)=0;   
                  
              end  
end

measurement_ohm_10K_2(i,1) = (a_10_2(i)*Datafile(i,4)) + b_10_2(i);

% ************************************************************************** 
% Check the real values to check if above point is after the 400 ohm infection                                              

for s = 5:1:29       
              Condition1= le(Datafile(i,2),Avg_Freq_10K(s,2));            % Comparing real code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,2),Avg_Freq_10K(s+1,2));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                   a_r_10_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,2)-Avg_Freq_10K(Index+1,2))); % calculate slope for interpolation
                    b_r_10_2(i)=  ImpedanceSequence(Index)- (a_r_10_2(i)*Avg_Freq_10K(Index,2)) ;                                             % calculate intercept for interpolation
                    
                    break;                                                         % store the index
              else a_r_10_2(i) =0; b_r_10_2(i)=0;    
              end 
  end

  real_10K_2(i,1) = (a_r_10_2(i)*Datafile(i,2)) + b_r_10_2(i);



    if (abs(real_10K_2(i,1)-measurement_ohm_10K_2(i,1))> 0)

            if(abs(real_10K_2(i,1)- measurement_ohm_10K_2(i,1))<= 600)
            measured_ohms(i,1)= measurement_ohm_10K_2(i,1);
            end

    end 
 
 end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationWB_10 = ohms(:,1);
wristupdownWB_10 = ohms(:,2);
armrotationWB_10 = ohms(:,3);
armupdownWB_10 = ohms(:,4);
anklerotationWB_10 = ohms(:,5);
ankleupdownWB_10 = ohms(:,6);
legmoveWB_10 = ohms(:,7);
randomWB_10 = ohms(:,8);

save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['wristupdown',s1,'_',num,num2],['wristupdown',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['armupdown',s1,'_',num,num2],['armupdown',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['ankleupdown',s1,'_',num,num2],['ankleupdown',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);

x1 = ['wristrotation',s2,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s2,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s2,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s2,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s2,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s2,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s2,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s2,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j=0

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

%% 10kHz
 for i = 1:1:40                                                               % Go through all data points for 10kHZ
%*************************************************************************%        
% Calculate if data poit is before 400 ohm 
for s = 1:1:4                                                                 % impedance inflection at 400 ohm
        
              Condition1 = gt(Datafile(i,4),Avg_Freq_10K(s,4))  ;              % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_10K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_10_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,4)-Avg_Freq_10K(Index+1,4))); % calculate slope for interpolation
                    b_10_1(i)=  ImpedanceSequence(Index)- (a_10_1(i)*Avg_Freq_10K(Index,4));                                              % calculate intercept for interpolation
                    
                    break;                                                         % store the index
              else a_10_1(i) =0; b_10_1(i)=0; 
                 
              end
              
end
 measurement_ohm_10K_1(i,1) = (a_10_1(i)*Datafile(i,4)) + b_10_1(i);
 
% Check the real values to check if above point is before the 400 ohm infection        
   for s = 1:1:4                                                         
        
              Condition1= le(Datafile(i,2),Avg_Freq_10K(s,2)) ;           % Comparing real code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,2),Avg_Freq_10K(s+1,2));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_r_10_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,2)-Avg_Freq_10K(Index+1,2))) ;% calculate slope for interpolation
                    b_r_10_1(i)=  ImpedanceSequence(Index)- (a_r_10_1(i)*Avg_Freq_10K(Index,2));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_r_10_1(i) =0; b_r_10_1(i)=0;    
              end
            
   end
   real_10K_1(i,1) = (a_r_10_1(i)*Datafile(i,2)) + b_r_10_1(i) ; 
   
   if (real_10K_1(i,1)-measurement_ohm_10K_1(i,1)> 0)
        
        if(real_10K_1(i,1)- measurement_ohm_10K_1(i,1)<= 600)
        measured_ohms(i,1)= measurement_ohm_10K_1(i,1);
        end
        
    end

%*************************************************************************%     

for s = 5:1:29                                                        % impedance inflection after 400 ohm
        
              Condition1= le(Datafile(i,4),Avg_Freq_10K(s,4))    ;              % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_10K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_10_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,4)-Avg_Freq_10K(Index+1,4))); % calculate slope for interpolation
                    b_10_2(i)=  ImpedanceSequence(Index)- (a_10_2(i)*Avg_Freq_10K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_10_2(i) =0; b_10_2(i)=0;   
                  
              end  
end

measurement_ohm_10K_2(i,1) = (a_10_2(i)*Datafile(i,4)) + b_10_2(i);

% ************************************************************************** 
% Check the real values to check if above point is after the 400 ohm infection                                              

for s = 5:1:29       
              Condition1= le(Datafile(i,2),Avg_Freq_10K(s,2));            % Comparing real code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,2),Avg_Freq_10K(s+1,2));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                   a_r_10_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,2)-Avg_Freq_10K(Index+1,2))); % calculate slope for interpolation
                    b_r_10_2(i)=  ImpedanceSequence(Index)- (a_r_10_2(i)*Avg_Freq_10K(Index,2)) ;                                             % calculate intercept for interpolation
                    
                    break;                                                         % store the index
              else a_r_10_2(i) =0; b_r_10_2(i)=0;    
              end 
  end

  real_10K_2(i,1) = (a_r_10_2(i)*Datafile(i,2)) + b_r_10_2(i);



    if (abs(real_10K_2(i,1)-measurement_ohm_10K_2(i,1))> 0)

            if(abs(real_10K_2(i,1)- measurement_ohm_10K_2(i,1))<= 600)
            measured_ohms(i,1)= measurement_ohm_10K_2(i,1);
            end

    end 
 
 end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationArm_10 = ohms(:,1);
wristupdownArm_10 = ohms(:,2);
armrotationArm_10 = ohms(:,3);
armupdownArm_10 = ohms(:,4);
anklerotationArm_10 = ohms(:,5);
ankleupdownArm_10 = ohms(:,6);
legmoveArm_10 = ohms(:,7);
randomArm_10 = ohms(:,8);


save(['wristrotation',s2,'_',num,num2],['wristrotation',s2,'_',num,num2]);
save(['wristupdown',s2,'_',num,num2],['wristupdown',s2,'_',num,num2]);
save(['armrotation',s2,'_',num,num2],['armrotation',s2,'_',num,num2]);
save(['armupdown',s2,'_',num,num2],['armupdown',s2,'_',num,num2]);
save(['anklerotation',s2,'_',num,num2],['anklerotation',s2,'_',num,num2]);
save(['ankleupdown',s2,'_',num,num2],['ankleupdown',s2,'_',num,num2]);
save(['legmove',s2,'_',num,num2],['legmove',s2,'_',num,num2]);
save(['random',s2,'_',num,num2],['random',s2,'_',num,num2]);

%%
x1 = ['wristrotation',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s3,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s3,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s3,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s3,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j = 0

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

%% 10kHz
 for i = 1:1:40                                                               % Go through all data points for 10kHZ
%*************************************************************************%        
% Calculate if data poit is before 400 ohm 
for s = 1:1:4                                                                 % impedance inflection at 400 ohm
        
              Condition1 = gt(Datafile(i,4),Avg_Freq_10K(s,4))  ;              % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_10K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_10_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,4)-Avg_Freq_10K(Index+1,4))); % calculate slope for interpolation
                    b_10_1(i)=  ImpedanceSequence(Index)- (a_10_1(i)*Avg_Freq_10K(Index,4));                                              % calculate intercept for interpolation
                    
                    break;                                                         % store the index
              else a_10_1(i) =0; b_10_1(i)=0; 
                 
              end
              
end
 measurement_ohm_10K_1(i,1) = (a_10_1(i)*Datafile(i,4)) + b_10_1(i);
 
% Check the real values to check if above point is before the 400 ohm infection        
   for s = 1:1:4                                                         
        
              Condition1= le(Datafile(i,2),Avg_Freq_10K(s,2)) ;           % Comparing real code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,2),Avg_Freq_10K(s+1,2));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_r_10_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,2)-Avg_Freq_10K(Index+1,2))) ;% calculate slope for interpolation
                    b_r_10_1(i)=  ImpedanceSequence(Index)- (a_r_10_1(i)*Avg_Freq_10K(Index,2));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_r_10_1(i) =0; b_r_10_1(i)=0;    
              end
            
   end
   real_10K_1(i,1) = (a_r_10_1(i)*Datafile(i,2)) + b_r_10_1(i) ; 
   
   if (real_10K_1(i,1)-measurement_ohm_10K_1(i,1)> 0)
        
        if(real_10K_1(i,1)- measurement_ohm_10K_1(i,1)<= 600)
        measured_ohms(i,1)= measurement_ohm_10K_1(i,1);
        end
        
    end

%*************************************************************************%     

for s = 5:1:29                                                        % impedance inflection after 400 ohm
        
              Condition1= le(Datafile(i,4),Avg_Freq_10K(s,4))    ;              % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_10K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_10_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,4)-Avg_Freq_10K(Index+1,4))); % calculate slope for interpolation
                    b_10_2(i)=  ImpedanceSequence(Index)- (a_10_2(i)*Avg_Freq_10K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_10_2(i) =0; b_10_2(i)=0;   
                  
              end  
end

measurement_ohm_10K_2(i,1) = (a_10_2(i)*Datafile(i,4)) + b_10_2(i);

% ************************************************************************** 
% Check the real values to check if above point is after the 400 ohm infection                                              

for s = 5:1:29       
              Condition1= le(Datafile(i,2),Avg_Freq_10K(s,2));            % Comparing real code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,2),Avg_Freq_10K(s+1,2));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                   a_r_10_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_10K(Index,2)-Avg_Freq_10K(Index+1,2))); % calculate slope for interpolation
                    b_r_10_2(i)=  ImpedanceSequence(Index)- (a_r_10_2(i)*Avg_Freq_10K(Index,2)) ;                                             % calculate intercept for interpolation
                    
                    break;                                                         % store the index
              else a_r_10_2(i) =0; b_r_10_2(i)=0;    
              end 
  end

  real_10K_2(i,1) = (a_r_10_2(i)*Datafile(i,2)) + b_r_10_2(i);



    if (abs(real_10K_2(i,1)-measurement_ohm_10K_2(i,1))> 0)

            if(abs(real_10K_2(i,1)- measurement_ohm_10K_2(i,1))<= 600)
            measured_ohms(i,1)= measurement_ohm_10K_2(i,1);
            end

    end 
 
 end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationLeg_10 = ohms(:,1);
wristupdownLeg_10 = ohms(:,2);
armrotationLeg_10 = ohms(:,3);
armupdownLeg_10 = ohms(:,4);
anklerotationLeg_10 = ohms(:,5);
ankleupdownLeg_10 = ohms(:,6);
legmoveLeg_10 = ohms(:,7);
randomLeg_10 = ohms(:,8);

save(['wristrotation',s3,'_',num,num2],['wristrotation',s3,'_',num,num2]);
save(['wristupdown',s3,'_',num,num2],['wristupdown',s3,'_',num,num2]);
save(['armrotation',s3,'_',num,num2],['armrotation',s3,'_',num,num2]);
save(['armupdown',s3,'_',num,num2],['armupdown',s3,'_',num,num2]);
save(['anklerotation',s3,'_',num,num2],['anklerotation',s3,'_',num,num2]);
save(['ankleupdown',s3,'_',num,num2],['ankleupdown',s3,'_',num,num2]);
save(['legmove',s3,'_',num,num2],['legmove',s3,'_',num,num2]);
save(['random',s3,'_',num,num2],['random',s3,'_',num,num2]);