num = num2str(6);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;


load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')

ohms = [];

x1 = ['wristrotation',s1,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s1,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s1,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s1,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s1,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s1,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s1,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s1,'_',num,num2];
x_8 = eval(x8);
x_m = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

for m = 1:8
    Datafile_temp=[];
    Datafile =[];
Datafile_temp = x_m(:,1+j*3:3+j*3);


for k = 1:1:40
Datafile_temp(k,4)= sqrt(Datafile_temp(k,2).^2 + Datafile_temp(k,3).^2);
end
columnextra = zeros(length(Datafile_temp(:,1)),1);

Datafile = [columnextra,Datafile_temp];

%% 60KhZ
%--------------------------- ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_60K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_60= vq1';
%--------------- TILL Here **************************************************************************

% n=0
%  for n = 0:10:(length(Impedance) - 10)
 for i = 1:1:length(Datafile)
     
     
% ----------------------------ADD THIS************************************************************************** 
           if (Datafile(i,3)< min(aVG_60))
                Datafile(i,3)=Datafile(i,3)+400;
            end
             for s = 1:1:15                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Datafile(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Datafile(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*Datafile(i,3)) + b_60_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:15                                                      
        
      Condition1 = le(Datafile(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*Datafile(i,4)) + b_i_60_1(i)  

   if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 1600)
            measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
            Datafile(i, 6) = measurement_ohm_60K_1(i,1);
           if(measurement_ohm_60K_1(i,1)==0)
               measured_ohms(i,1)= img_60K_1(i,1);
% <--------- CHANGE THIS VARIABLE "Impedance_Arm_60K_S006_S010" to the array where raw data to be converted is stored
               Datafile(i, 6) =img_60K_1(i,1); 
           end
       
        end

   end

% % Section 2
for s = 16:1:29                                                    % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Datafile(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Datafile(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
            end
         measurement_ohm_60K_2(i,1) = (a_60_2(i)*Datafile(i,3)) + b_60_2(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 16:1:29                                                     
        
      Condition1 = le(Datafile(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*Datafile(i,4)) + b_i_60_2(i)  

   if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 500)
            measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
            Datafile(i, 6) = measurement_ohm_60K_2(i,1);
           if(measurement_ohm_60K_2(i,1)==0)
               measured_ohms(i,1)= img_60K_2(i,1);
               Datafile(i, 6) =img_60K_2(i,1);
           end
       
        end
        
   end 
  
   
 % ---------------------------------TILL HERE ***********************************************************  
 end
%  end


ohms(:,m) = measured_ohms(:,1);
j = j+1;
end
    
wristrotationWB_60 = ohms(:,1);
wristupdownWB_60 = ohms(:,2);
armrotationWB_60 = ohms(:,3);
armupdownWB_60 = ohms(:,4);
anklerotationWB_60 = ohms(:,5);
ankleupdownWB_60 = ohms(:,6);
legmoveWB_60 = ohms(:,7);
randomWB_60 = ohms(:,8);
 
save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['wristupdown',s1,'_',num,num2],['wristupdown',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['armupdown',s1,'_',num,num2],['armupdown',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['ankleupdown',s1,'_',num,num2],['ankleupdown',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);
close all

