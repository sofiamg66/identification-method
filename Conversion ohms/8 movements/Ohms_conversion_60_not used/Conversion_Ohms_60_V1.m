%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')

num = num2str(6);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
s = s1;
%%

x1 = ['wristrotation',s,'_',num,num2];
x_1 = eval(x1);
x2 = ['wristupdown',s,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);


%for k = 1:1:100
for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

%% 60 kHz

for i = 1:1:40
% for i =16:1:18
 %***************************************************************************        
        
 for s = 1:1:20                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_60K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_60K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,4)-Avg_Freq_60K(Index+1,4))); % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*Avg_Freq_60K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*Datafile(i,4)) + b_60_1(i); 

% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:20                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_60K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_60K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))); % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*Datafile(i,3)) + b_i_60_1(i);  



if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
        end
        
    end 
     

%***************************************************************************     

for s = 21:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_60K(s,4));               % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_60K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,4)-Avg_Freq_60K(Index+1,4))); % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*Avg_Freq_60K(Index,4));                                              % calculate intercept for interpolation
                    break;                                                       % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
     end
            measurement_ohm_60K_2(i,1) = (a_60_2(i)*Datafile(i,1)) + b_60_2(i);
   

 
 % Check the imaginary values to check if above point is before the 400 ohm infection        

for s = 21:1:29                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_60K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_60K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))); % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*Datafile(i,3)) + b_i_60_2(i);  




if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
        end
        
    end 
      
end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationWB_60 = ohms(:,1);
wristupdownWB_60 = ohms(:,2);
armrotationWB_60 = ohms(:,3);
armupdownWB_60 = ohms(:,4);
anklerotationWB_60 = ohms(:,5);
ankleupdownWB_60 = ohms(:,6);
legmoveWB_60 = ohms(:,7);
randomWB_60 = ohms(:,8);

save('wristrotationWB_60.mat','wristrotationWB_60');
save('wristupdownWB_60','wristupdownWB_60');
save('armrotationWB_60','armrotationWB_60');
save('armupdownWB_60','armupdownWB_60');
save('anklerotationWB_60','anklerotationWB_60');
save('ankleupdownWB_60','ankleupdownWB_60');
save('legmoveWB_60','legmoveWB_60');
save('randomWB_60','randomWB_60');

x1 = ['wristrotation',s2,'_',num,num2];
x_1 = eval(x1);
x2 = ['wristupdown',s2,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s2,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s2,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s2,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s2,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s2,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s2,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j=0


for m = 1:8
Datafile = x(:,1+j*3:3+j*3);


%for k = 1:1:100
for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

%% 60 kHz

for i = 1:1:40
% for i =16:1:18
 %***************************************************************************        
        
 for s = 1:1:20                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_60K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_60K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,4)-Avg_Freq_60K(Index+1,4))); % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*Avg_Freq_60K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*Datafile(i,4)) + b_60_1(i); 

% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:20                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_60K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_60K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))); % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*Datafile(i,3)) + b_i_60_1(i);  



if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
        end
        
    end 
     

%***************************************************************************     

for s = 21:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_60K(s,4));               % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_60K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,4)-Avg_Freq_60K(Index+1,4))); % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*Avg_Freq_60K(Index,4));                                              % calculate intercept for interpolation
                    break;                                                       % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
     end
            measurement_ohm_60K_2(i,1) = (a_60_2(i)*Datafile(i,1)) + b_60_2(i);
   

 
 % Check the imaginary values to check if above point is before the 400 ohm infection        

for s = 21:1:29                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_60K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_60K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))); % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*Datafile(i,3)) + b_i_60_2(i);  




if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
        end
        
    end 
      
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationArm_60 = ohms(:,1);
wristupdownArm_60 = ohms(:,2);
armrotationArm_60 = ohms(:,3);
armupdownArm_60 = ohms(:,4);
anklerotationArm_60 = ohms(:,5);
ankleupdownArm_60 = ohms(:,6);
legmoveArm_60 = ohms(:,7);
randomArm_60 = ohms(:,8);

save('wristrotationArm_60','wristrotationArm_60');
save('wristupdownArm_60','wristupdownArm_60');
save('armrotationArm_60','armrotationArm_60');
save('armupdownArm_60','armupdownArm_60');
save('anklerotationArm_60','anklerotationArm_60');
save('ankleupdownArm_60','ankleupdownArm_60');
save('legmoveArm_60','legmoveArm_60');
save('randomArm_60','randomArm_60');
%%
x1 = ['wristrotation',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['wristupdown',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s3,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s3,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s3,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s3,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j = 0

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);


%for k = 1:1:100
for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

%% 60 kHz

for i = 1:1:40
% for i =16:1:18
 %***************************************************************************        
        
 for s = 1:1:20                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_60K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_60K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,4)-Avg_Freq_60K(Index+1,4))); % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*Avg_Freq_60K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*Datafile(i,4)) + b_60_1(i); 

% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:20                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_60K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_60K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))); % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*Datafile(i,3)) + b_i_60_1(i);  



if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
        end
        
    end 
     

%***************************************************************************     

for s = 21:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_60K(s,4));               % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_60K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,4)-Avg_Freq_60K(Index+1,4))); % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*Avg_Freq_60K(Index,4));                                              % calculate intercept for interpolation
                    break;                                                       % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
     end
            measurement_ohm_60K_2(i,1) = (a_60_2(i)*Datafile(i,1)) + b_60_2(i);
   

 
 % Check the imaginary values to check if above point is before the 400 ohm infection        

for s = 21:1:29                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_60K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_60K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))); % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*Datafile(i,3)) + b_i_60_2(i);  




if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
        end
        
    end 
      
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationLeg_60 = ohms(:,1);
wristupdownLeg_60 = ohms(:,2);
armrotationLeg_60 = ohms(:,3);
armupdownLeg_60 = ohms(:,4);
anklerotationLeg_60 = ohms(:,5);
ankleupdownLeg_60 = ohms(:,6);
legmoveLeg_60 = ohms(:,7);
randomLeg_60 = ohms(:,8);

save('wristrotationLeg_60','wristrotationLeg_60');
save('wristupdownLeg_60','wristupdownLeg_60');
save('armrotationLeg_60','armrotationLeg_60');
save('armupdownLeg_60','armupdownLeg_60');
save('anklerotationLeg_60','anklerotationLeg_60');
save('ankleupdownLeg_60','ankleupdownLeg_60')
save('legmoveLeg_60','legmoveLeg_60')
save('randomLeg_60','randomLeg_60')