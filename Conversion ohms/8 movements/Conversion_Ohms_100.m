
%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(1);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;
%%
x1 = ['wristrotation',s1,'_',num,num2,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s1,'_',num,num2,num2];
x_2 = eval(x2);
x3 = ['armrotation',s1,'_',num,num2,num2];
x_3 = eval(x3);
x4 = ['armupdown',s1,'_',num,num2,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s1,'_',num,num2,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s1,'_',num,num2,num2];
x_6 = eval(x6);
x7 = ['legmove',s1,'_',num,num2,num2];
x_7 = eval(x7);
x8 = ['random',s1,'_',num,num2,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

    for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

%for k = 1:1:100
for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 100 kHz
for i = 1:1:40
%for i = 28:1:30
%         for i = 28:1:30
%***************************************************************************        
            for s = 1:1:8                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_100K(s,4))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_100K(s+1,4))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_100_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,2)-Avg_Freq_100K(Index+1,2)));% calculate slope for interpolation
                    b_100_1(i)=  ImpedanceSequence(Index)- (a_100_1(i)*Avg_Freq_100K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_100_1(i) =0; b_100_1(i)=0;    
              end
            end
         measurement_ohm_100K_1(i,1) = (a_100_1(i)*Datafile(i,4)) + b_100_1(i) 

         % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 1:1:8                                                        
        
      Condition1 = le(Datafile(i,3),Avg_Freq_100K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,3),Avg_Freq_100K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_100_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,3)-Avg_Freq_100K(Index+1,3))) % calculate slope for interpolation
                    b_i_100_1(i)=  ImpedanceSequence(Index)- (a_i_100_1(i)*Avg_Freq_100K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_100_1(i) =0; b_i_100_1(i)=0;    
              end
            
   end
   img_100K_1(i,1) = (a_i_100_1(i)*Datafile(i,3)) + b_i_100_1(i)  



    if (abs(img_100K_1(i,1)-measurement_ohm_100K_1(i,1))> 0)
        
        if(abs(img_100K_1(i,1)- measurement_ohm_100K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_100K_1(i,1);
        end
        
    end       
%***************************************************************************     
             for s = 9:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_100K(s,4))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_100K(s+1,4))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_100_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,4)-Avg_Freq_100K(Index+1,4))) % calculate slope for interpolation
                    b_100_2(i)=  ImpedanceSequence(Index)- (a_100_2(i)*Avg_Freq_100K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_100_2(i) =0; b_100_2(i)=0;    
              end
             end
            measurement_ohm_100K_2(i,1) = (a_100_2(i)*Datafile(i,4)) + b_100_2(i)

% Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 9:1:29                                                        
        
      Condition1 = le(Datafile(i,3),Avg_Freq_100K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,3),Avg_Freq_100K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_100_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,3)-Avg_Freq_100K(Index+1,3))) % calculate slope for interpolation
                    b_i_100_2(i)=  ImpedanceSequence(Index)- (a_i_100_2(i)*Avg_Freq_100K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_100_2(i) =0; b_i_100_2(i)=0;    
              end
            
   end
   img_100K_2(i,1) = (a_i_100_2(i)*Datafile(i,3)) + b_i_100_2(i)  

    if (abs(img_100K_2(i,1)-measurement_ohm_100K_2(i,1))> 0)
        
        if(abs(img_100K_2(i,1)- measurement_ohm_100K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_100K_2(i,1);
        end
        
    end       
end
ohms(:,m) = measured_ohms;
j = j+1;
    end
    %me falta poner el nombre a la variable
    %save(x1,x(:,1));
    
wristrotationWB_100 = ohms(:,1);
wristupdownWB_100 = ohms(:,2);
armrotationWB_100 = ohms(:,3);
armupdownWB_100 = ohms(:,4);
anklerotationWB_100 = ohms(:,5);
ankleupdownWB_100 = ohms(:,6);
legmoveWB_100 = ohms(:,7);
randomWB_100 = ohms(:,8);

%save('wristrotationWB_100.mat','wristrotationWB_100');
%save('wristupdownWB_100','wristupdownWB_100');
%save('armrotationWB_100','armrotationWB_100');
%save('armupdownWB_100','armupdownWB_100');
%save('anklerotationWB_100','anklerotationWB_100');
%save('ankleupdownWB_100','ankleupdownWB_100');
%save('legmoveWB_100','legmoveWB_100');
%save('randomWB_100','randomWB_100');

j = 0;

x1 = ['wristrotation',s2,'_',num,num2,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s2,'_',num,num2,num2];
x_2 = eval(x2);
x3 = ['armrotation',s2,'_',num,num2,num2];
x_3 = eval(x3);
x4 = ['armupdown',s2,'_',num,num2,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s2,'_',num,num2,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s2,'_',num,num2,num2];
x_6 = eval(x6);
x7 = ['legmove',s2,'_',num,num2,num2];
x_7 = eval(x7);
x8 = ['random',s2,'_',num,num2,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

    for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

%for k = 1:1:100
for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 100 kHz
for i = 1:1:40
%for i = 28:1:30
%         for i = 28:1:30
%***************************************************************************        
            for s = 1:1:8                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_100K(s,4))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_100K(s+1,4))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_100_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,2)-Avg_Freq_100K(Index+1,2)));% calculate slope for interpolation
                    b_100_1(i)=  ImpedanceSequence(Index)- (a_100_1(i)*Avg_Freq_100K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_100_1(i) =0; b_100_1(i)=0;    
              end
            end
         measurement_ohm_100K_1(i,1) = (a_100_1(i)*Datafile(i,4)) + b_100_1(i) 

         % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 1:1:8                                                        
        
      Condition1 = le(Datafile(i,3),Avg_Freq_100K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,3),Avg_Freq_100K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_100_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,3)-Avg_Freq_100K(Index+1,3))) % calculate slope for interpolation
                    b_i_100_1(i)=  ImpedanceSequence(Index)- (a_i_100_1(i)*Avg_Freq_100K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_100_1(i) =0; b_i_100_1(i)=0;    
              end
            
   end
   img_100K_1(i,1) = (a_i_100_1(i)*Datafile(i,3)) + b_i_100_1(i)  



    if (abs(img_100K_1(i,1)-measurement_ohm_100K_1(i,1))> 0)
        
        if(abs(img_100K_1(i,1)- measurement_ohm_100K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_100K_1(i,1);
        end
        
    end       
%***************************************************************************     
             for s = 9:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_100K(s,4))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_100K(s+1,4))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_100_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,4)-Avg_Freq_100K(Index+1,4))) % calculate slope for interpolation
                    b_100_2(i)=  ImpedanceSequence(Index)- (a_100_2(i)*Avg_Freq_100K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_100_2(i) =0; b_100_2(i)=0;    
              end
             end
            measurement_ohm_100K_2(i,1) = (a_100_2(i)*Datafile(i,4)) + b_100_2(i)

% Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 9:1:29                                                        
        
      Condition1 = le(Datafile(i,3),Avg_Freq_100K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,3),Avg_Freq_100K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_100_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,3)-Avg_Freq_100K(Index+1,3))) % calculate slope for interpolation
                    b_i_100_2(i)=  ImpedanceSequence(Index)- (a_i_100_2(i)*Avg_Freq_100K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_100_2(i) =0; b_i_100_2(i)=0;    
              end
            
   end
   img_100K_2(i,1) = (a_i_100_2(i)*Datafile(i,3)) + b_i_100_2(i)  

    if (abs(img_100K_2(i,1)-measurement_ohm_100K_2(i,1))> 0)
        
        if(abs(img_100K_2(i,1)- measurement_ohm_100K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_100K_2(i,1);
        end
        
    end       
end
ohms(:,m) = measured_ohms;
j = j+1;
end
wristrotationArm_100 = ohms(:,1);
wristupdownArm_100 = ohms(:,2);
armrotationArm_100 = ohms(:,3);
armupdownArm_100 = ohms(:,4);
anklerotationArm_100 = ohms(:,5);
ankleupdownArm_100 = ohms(:,6);
legmoveArm_100 = ohms(:,7);
randomArm_100 = ohms(:,8);


%save('wristrotationArm_100','wristrotationArm_100');
%save('wristupdownArm_100','wristupdownArm_100');
%save('armrotationArm_100','armrotationArm_100');
%save('armupdownArm_100','armupdownArm_100');
%save('anklerotationArm_100','anklerotationArm_100');
%save('ankleupdownArm_100','ankleupdownArm_100');
%save('legmoveArm_100','legmoveArm_100');
%save('randomArm_100','randomArm_100');
%%

j = 0;

x1 = ['wristrotation',s3,'_',num,num2,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s3,'_',num,num2,num2];
x_2 = eval(x2);
x3 = ['armrotation',s3,'_',num,num2,num2];
x_3 = eval(x3);
x4 = ['armupdown',s3,'_',num,num2,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s3,'_',num,num2,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s3,'_',num,num2,num2];
x_6 = eval(x6);
x7 = ['legmove',s3,'_',num,num2,num2];
x_7 = eval(x7);
x8 = ['random',s3,'_',num,num2,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

    for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

%for k = 1:1:100
for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 100 kHz
for i = 1:1:40
%for i = 28:1:30
%         for i = 28:1:30
%***************************************************************************        
            for s = 1:1:8                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),Avg_Freq_100K(s,4))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),Avg_Freq_100K(s+1,4))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_100_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,2)-Avg_Freq_100K(Index+1,2)));% calculate slope for interpolation
                    b_100_1(i)=  ImpedanceSequence(Index)- (a_100_1(i)*Avg_Freq_100K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_100_1(i) =0; b_100_1(i)=0;    
              end
            end
         measurement_ohm_100K_1(i,1) = (a_100_1(i)*Datafile(i,4)) + b_100_1(i) 

         % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 1:1:8                                                        
        
      Condition1 = le(Datafile(i,3),Avg_Freq_100K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,3),Avg_Freq_100K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_100_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,3)-Avg_Freq_100K(Index+1,3))) % calculate slope for interpolation
                    b_i_100_1(i)=  ImpedanceSequence(Index)- (a_i_100_1(i)*Avg_Freq_100K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_100_1(i) =0; b_i_100_1(i)=0;    
              end
            
   end
   img_100K_1(i,1) = (a_i_100_1(i)*Datafile(i,3)) + b_i_100_1(i)  



    if (abs(img_100K_1(i,1)-measurement_ohm_100K_1(i,1))> 0)
        
        if(abs(img_100K_1(i,1)- measurement_ohm_100K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_100K_1(i,1);
        end
        
    end       
%***************************************************************************     
             for s = 9:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,4),Avg_Freq_100K(s,4))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,4),Avg_Freq_100K(s+1,4))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_100_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,4)-Avg_Freq_100K(Index+1,4))) % calculate slope for interpolation
                    b_100_2(i)=  ImpedanceSequence(Index)- (a_100_2(i)*Avg_Freq_100K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_100_2(i) =0; b_100_2(i)=0;    
              end
             end
            measurement_ohm_100K_2(i,1) = (a_100_2(i)*Datafile(i,4)) + b_100_2(i)

% Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 9:1:29                                                        
        
      Condition1 = le(Datafile(i,3),Avg_Freq_100K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Datafile(i,3),Avg_Freq_100K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_100_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_100K(Index,3)-Avg_Freq_100K(Index+1,3))) % calculate slope for interpolation
                    b_i_100_2(i)=  ImpedanceSequence(Index)- (a_i_100_2(i)*Avg_Freq_100K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_100_2(i) =0; b_i_100_2(i)=0;    
              end
            
   end
   img_100K_2(i,1) = (a_i_100_2(i)*Datafile(i,3)) + b_i_100_2(i)  

    if (abs(img_100K_2(i,1)-measurement_ohm_100K_2(i,1))> 0)
        
        if(abs(img_100K_2(i,1)- measurement_ohm_100K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_100K_2(i,1);
        end
        
    end       
end
ohms(:,m) = measured_ohms;
j = j+1;
    end
    
    
wristrotationLeg_100 = ohms(:,1);
wristupdownLeg_100 = ohms(:,2);
armrotationLeg_100 = ohms(:,3);
armupdownLeg_100 = ohms(:,4);
anklerotationLeg_100 = ohms(:,5);
ankleupdownLeg_100 = ohms(:,6);
legmoveLeg_100 = ohms(:,7);
randomLeg_100 = ohms(:,8);
    
save('wristrotationLeg_100','wristrotationLeg_100');
save('wristupdownLeg_100','wristupdownLeg_100');
save('armrotationLeg_100','armrotationLeg_100');
save('armupdownLeg_100','armupdownLeg_100');
save('anklerotationLeg_100','anklerotationLeg_100');
save('ankleupdownLeg_100','ankleupdownLeg_100')
save('legmoveLeg_100','legmoveLeg_100')
save('randomLeg_100','randomLeg_100')