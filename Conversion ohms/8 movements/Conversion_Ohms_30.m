%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(3);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;
%%

x1 = ['wristrotation',s1,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s1,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s1,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s1,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s1,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s1,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s1,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s1,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 20KhZ to 50khz
BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2 why is n = 2?
n = 1;
for j_P = 30   % select the correct frequency file ( 10K, 20K....100K)
      FileName=[BaseName1,num2str(j_P),BaseName2];
      temp_array=eval(FileName);
      
  for i = 40*n-39:1:40*n
          for s = 1:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),temp_array(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),temp_array(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                    b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                   break;     % store the index
              else a(i) =0; b(i)=0;    
              end
        end
         measurement_ohm_20_50K_1(i,1) = (a(i)*Datafile(i,4)) + b(i) ;
         measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
        end
         n= n+1;
end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationWB_30 = ohms(:,1);
wristupdownWB_30 = ohms(:,2);
armrotationWB_30 = ohms(:,3);
armupdownWB_30 = ohms(:,4);
anklerotationWB_30 = ohms(:,5);
ankleupdownWB_30 = ohms(:,6);
legmoveWB_30 = ohms(:,7);
randomWB_30 = ohms(:,8);

save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['wristupdown',s1,'_',num,num2],['wristupdown',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['armupdown',s1,'_',num,num2],['armupdown',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['ankleupdown',s1,'_',num,num2],['ankleupdown',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);

x1 = ['wristrotation',s2,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s2,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s2,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s2,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s2,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s2,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s2,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s2,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j=0

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 20KhZ to 50khz
BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2 why is n = 2?
n = 1;
for j_P = 30   % select the correct frequency file ( 10K, 20K....100K)
      FileName=[BaseName1,num2str(j_P),BaseName2];
      temp_array=eval(FileName);
      
  for i = 40*n-39:1:40*n
          for s = 1:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),temp_array(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),temp_array(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                    b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                   break;     % store the index
              else a(i) =0; b(i)=0;    
              end
        end
         measurement_ohm_20_50K_1(i,1) = (a(i)*Datafile(i,4)) + b(i) ;
         measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
        end
         n= n+1;
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationArm_30 = ohms(:,1);
wristupdownArm_30 = ohms(:,2);
armrotationArm_30 = ohms(:,3);
armupdownArm_30 = ohms(:,4);
anklerotationArm_30 = ohms(:,5);
ankleupdownArm_30 = ohms(:,6);
legmoveArm_30 = ohms(:,7);
randomArm_30 = ohms(:,8);


save(['wristrotation',s2,'_',num,num2],['wristrotation',s2,'_',num,num2]);
save(['wristupdown',s2,'_',num,num2],['wristupdown',s2,'_',num,num2]);
save(['armrotation',s2,'_',num,num2],['armrotation',s2,'_',num,num2]);
save(['armupdown',s2,'_',num,num2],['armupdown',s2,'_',num,num2]);
save(['anklerotation',s2,'_',num,num2],['anklerotation',s2,'_',num,num2]);
save(['ankleupdown',s2,'_',num,num2],['ankleupdown',s2,'_',num,num2]);
save(['legmove',s2,'_',num,num2],['legmove',s2,'_',num,num2]);
save(['random',s2,'_',num,num2],['random',s2,'_',num,num2]);

%%
x1 = ['wristrotation',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['wrtistupdown',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s3,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s3,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s3,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s3,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];
j = 0

for m = 1:8
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:40
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 20KhZ to 50khz
BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2 why is n = 2?
n = 1;
for j_P = 30   % select the correct frequency file ( 10K, 20K....100K)
      FileName=[BaseName1,num2str(j_P),BaseName2];
      temp_array=eval(FileName);
      
  for i = 40*n-39:1:40*n
          for s = 1:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),temp_array(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),temp_array(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                    b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                   break;     % store the index
              else a(i) =0; b(i)=0;    
              end
        end
         measurement_ohm_20_50K_1(i,1) = (a(i)*Datafile(i,4)) + b(i) ;
         measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
        end
         n= n+1;
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationLeg_30 = ohms(:,1);
wristupdownLeg_30 = ohms(:,2);
armrotationLeg_30 = ohms(:,3);
armupdownLeg_30 = ohms(:,4);
anklerotationLeg_30 = ohms(:,5);
ankleupdownLeg_30 = ohms(:,6);
legmoveLeg_30 = ohms(:,7);
randomLeg_30 = ohms(:,8);

save(['wristrotation',s3,'_',num,num2],['wristrotation',s3,'_',num,num2]);
save(['wristupdown',s3,'_',num,num2],['wristupdown',s3,'_',num,num2]);
save(['armrotation',s3,'_',num,num2],['armrotation',s3,'_',num,num2]);
save(['armupdown',s3,'_',num,num2],['armupdown',s3,'_',num,num2]);
save(['anklerotation',s3,'_',num,num2],['anklerotation',s3,'_',num,num2]);
save(['ankleupdown',s3,'_',num,num2],['ankleupdown',s3,'_',num,num2]);
save(['legmove',s3,'_',num,num2],['legmove',s3,'_',num,num2]);
save(['random',s3,'_',num,num2],['random',s3,'_',num,num2]);