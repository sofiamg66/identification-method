load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(9);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;

%% Whole Body
x1 = ['wristrotate',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s3,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];
% x = [x_1];

%% 90 kHz
x_ = 1:1:30; 
v = Avg_Freq_90K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

% figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x_,v,xq,'linear','extrap')
% plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
% title('(Default) Linear EXTRApolation');

aVG_90= vq1';
figure()
plot(aVG_90)


%%
j=0;
for m = 1:1:5
 Datafile = x(:,1+j*3:3+j*3);
for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end

for i = 1:1:length(Datafile)  

 %***************************************************************************        
%  if (Impedance_Leg_90K_S006_S010(i,3)<min(aVG_90))
%                 Impedance_Leg_90K_S006_S010(i,3)=Impedance_Leg_90K_S006_S010(i,3)-1700;
%             end   
 for s = 1:1:13                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,2),aVG_90(s,1))                % Comparing REAL code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,2),aVG_90(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_90(Index,1)-aVG_90(Index+1,1))) % calculate slope for interpolation
                    b_90_1(i)=  ImpedanceSequence(Index)- (a_90_1(i)*aVG_90(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_1(i) =0; b_90_1(i)=0;    
              end
    end
         measurement_ohm_90K_1(i,1) = (a_90_1(i)*Datafile(i,2)) + b_90_1(i) 
   
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:13                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_90K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))) % calculate slope for interpolation
                    b_i_90_1(i)=  ImpedanceSequence(Index)- (a_i_90_1(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_1(i) =0; b_i_90_1(i)=0;    
              end
            
   end
   img_90K_1(i,1) = (a_i_90_1(i)*Datafile(i,3)) + b_i_90_1(i)  

 if (abs(img_90K_1(i,1)-measurement_ohm_90K_1(i,1))> 0)
        if(abs(img_90K_1(i,1)- measurement_ohm_90K_1(i,1))<= 1000)
%              measured_ohms(i,1)= measurement_ohm_90K_1(i,1);
              Datafile(i, 6) = measurement_ohm_90K_1(i,1);
          
            if(measurement_ohm_90K_1(i,1)==0)
%               measured_ohms(i,1)= img_90K_1(i,1);
                Datafile(i, 6) =img_90K_1(i,1);
            end
        end
 end
%     if (abs(img_90K_1(i,1)-measurement_ohm_90K_1(i,1))> 0)
%         if(abs(img_90K_1(i,1)- measurement_ohm_90K_1(i,1))<= 600)
%         measured_ohms(i,1)= measurement_ohm_90K_1(i,1);
%          Impedance_Leg_90K_S006_S010(i, 6) = measurement_ohm_90K_1(i,1);
%         end
%         
%     end 

 %***************************************************************************     
% Section 2
 for s = 14:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= gt(Datafile(i,2),aVG_90(s,1))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Datafile(i,2),aVG_90(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_90(Index,1)-aVG_90(Index+1,1))) % calculate slope for interpolation
                    b_90_2(i)=  ImpedanceSequence(Index)- (a_90_2(i)*aVG_90(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_90_2(i) =0; b_90_2(i)=0;    
              end
    end
            measurement_ohm_90K_2(i,1) = (a_90_2(i)*Datafile(i,3)) + b_90_2(i)
 
 % Check the imaginary values to check if above point is before the 400 ohm infection        
for s  = 14:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_90K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_90K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_90_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_90K(Index,3)-Avg_Freq_90K(Index+1,3))) % calculate slope for interpolation
                    b_i_90_2(i)=  ImpedanceSequence(Index)- (a_i_90_2(i)*Avg_Freq_90K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_90_2(i) =0; b_i_90_2(i)=0;    
              end
            
   end
   img_90K_2(i,1) = (a_i_90_2(i)*Datafile(i,3)) + b_i_90_2(i)  


 if (abs(img_90K_2(i,1)-measurement_ohm_90K_2(i,1))> 0)
        if(abs(img_90K_2(i,1)- measurement_ohm_90K_2(i,1))<= 1000)
%              measured_ohms(i,1)= measurement_ohm_90K_2(i,1);
              Datafile(i, 6) = measurement_ohm_90K_2(i,1);
          
            if(measurement_ohm_90K_2(i,1)==0)
%               measured_ohms(i,1)= img_90K_2(i,1);
                Datafile(i, 6) =img_90K_2(i,1);
            end
        end
%     if (abs(img_90K_2(i,1)-measurement_ohm_90K_2(i,1))> 0)
%         
%         if(abs(img_90K_2(i,1)- measurement_ohm_90K_2(i,1))<= 600)
%         measured_ohms(i,1)= measurement_ohm_90K_2(i,1);
%         Impedance_Leg_90K_S006_S010(i, 6) = measurement_ohm_90K_2(i,1);
%         end
%         
%     end  
end
end
ohms(:,m) = Datafile(:, 6);
j=j+1;
end

wristrotationLeg_90 = ohms(:,1);
armrotationLeg_90 = ohms(:,2);
anklerotationLeg_90 = ohms(:,3);
legmoveLeg_90 = ohms(:,4);
randomLeg_90 = ohms(:,5);
    
save('wristrotationLeg_90','wristrotationLeg_90');
save('armrotationLeg_90','armrotationLeg_90');
save('anklerotationLeg_90','anklerotationLeg_90');
save('legmoveLeg_90','legmoveLeg_90')
save('randomLeg_90','randomLeg_90')