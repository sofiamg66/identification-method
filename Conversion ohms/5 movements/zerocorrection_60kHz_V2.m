%% 60KhZ
load('ImpedanceSequence')
load('AVG_Freq_10K_100K_30values')

num = num2str(6);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';


WB_60K = vertcat(wristrotateWB_60,armrotateWB_60,anklerotateWB_60,legmoveWB_60,randomWB_60);
len = size(WB_60K);
vec = zeros(len(1),1);
WB_60K = [vec,WB_60K];

for i = 1:1:length(WB_60K)
WB_60K(i,5)= sqrt(WB_60K(i,3).^2+ WB_60K(i,4).^2);
end
%--------------------------- ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_60K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_60= vq1';
%--------------- TILL Here **************************************************************************

% n=0
%  for n = 0:10:(length(Impedance) - 10)
 for i = 1:1:length(WB_60K)
     
     
% ----------------------------ADD THIS************************************************************************** 
           if (WB_60K(i,3)< min(aVG_60))
                WB_60K(i,3)=WB_60K(i,3)+400;
            end
             for s = 1:1:15                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(WB_60K(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(WB_60K(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*WB_60K(i,3)) + b_60_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:15                                                      
        
      Condition1 = le(WB_60K(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(WB_60K(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*WB_60K(i,4)) + b_i_60_1(i)  

   if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 1600)
            measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
            WB_60K(i, 6) = measurement_ohm_60K_1(i,1);
           if(measurement_ohm_60K_1(i,1)==0)
               measured_ohms(i,1)= img_60K_1(i,1);
% <--------- CHANGE THIS VARIABLE "Impedance_Arm_60K_S006_S010" to the array where raw data to be converted is stored
               WB_60K(i, 6) =img_60K_1(i,1); 
           end
       
        end

   end

% % Section 2
for s = 16:1:29                                                    % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(WB_60K(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(WB_60K(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
            end
         measurement_ohm_60K_2(i,1) = (a_60_2(i)*WB_60K(i,3)) + b_60_2(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 16:1:29                                                     
        
      Condition1 = le(WB_60K(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(WB_60K(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*WB_60K(i,4)) + b_i_60_2(i)  

   if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 500)
            measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
            WB_60K(i, 6) = measurement_ohm_60K_2(i,1);
           if(measurement_ohm_60K_2(i,1)==0)
               measured_ohms(i,1)= img_60K_2(i,1);
               WB_60K(i, 6) =img_60K_2(i,1);
           end
       
        end
        
   end 
  
   
 % ---------------------------------TILL HERE ***********************************************************  
 end
%  end
wristrotationWB_60 = WB_60K(1:20,6);
armrotationWB_60 = WB_60K(21:40,6);
anklerotationWB_60 = WB_60K(41:60,6);
legmoveWB_60 = WB_60K(61:80,6);
randomWB_60 = WB_60K(81:100,6);

save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);

close all

Arm_60K = vertcat(wristrotateArm_60,armrotateArm_60,anklerotateArm_60,legmoveArm_60,randomArm_60);
len = size(Arm_60K);
vec = zeros(len(1),1);
Arm_60K = [vec,Arm_60K];

for i = 1:1:length(Arm_60K)
Arm_60K(i,5)= sqrt(Arm_60K(i,3).^2+ Arm_60K(i,4).^2);
end
%--------------------------- ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_60K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_60= vq1';
%--------------- TILL Here **************************************************************************

% n=0
%  for n = 0:10:(length(Impedance) - 10)
 for i = 1:1:length(Arm_60K)
     
     
% ----------------------------ADD THIS************************************************************************** 
           if (Arm_60K(i,3)< min(aVG_60))
                Arm_60K(i,3)=Arm_60K(i,3)+400;
            end
             for s = 1:1:15                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Arm_60K(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Arm_60K(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*Arm_60K(i,3)) + b_60_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:15                                                      
        
      Condition1 = le(Arm_60K(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Arm_60K(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*Arm_60K(i,4)) + b_i_60_1(i)  

   if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 1600)
            measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
            Arm_60K(i, 6) = measurement_ohm_60K_1(i,1);
           if(measurement_ohm_60K_1(i,1)==0)
               measured_ohms(i,1)= img_60K_1(i,1);
% <--------- CHANGE THIS VARIABLE "Impedance_Arm_60K_S006_S010" to the array where raw data to be converted is stored
               Arm_60K(i, 6) =img_60K_1(i,1); 
           end
       
        end

   end

% % Section 2
for s = 16:1:29                                                    % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Arm_60K(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Arm_60K(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
            end
         measurement_ohm_60K_2(i,1) = (a_60_2(i)*Arm_60K(i,3)) + b_60_2(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 16:1:29                                                     
        
      Condition1 = le(Arm_60K(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Arm_60K(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*Arm_60K(i,4)) + b_i_60_2(i)  

   if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 500)
            measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
            Arm_60K(i, 6) = measurement_ohm_60K_2(i,1);
           if(measurement_ohm_60K_2(i,1)==0)
               measured_ohms(i,1)= img_60K_2(i,1);
               Arm_60K(i, 6) =img_60K_2(i,1);
           end
       
        end
        
   end 
  
   
 % ---------------------------------TILL HERE ***********************************************************  
 end
%  end


wristrotationArm_60 = Arm_60K(1:20,6);
armrotationArm_60 = Arm_60K(21:40,6);
anklerotationArm_60 = Arm_60K(41:60,6);
legmoveArm_60 = Arm_60K(61:80,6);
randomArm_60 = Arm_60K(81:100,6);

save(['wristrotation',s2,'_',num,num2],['wristrotation',s2,'_',num,num2]);
save(['armrotation',s2,'_',num,num2],['armrotation',s2,'_',num,num2]);
save(['anklerotation',s2,'_',num,num2],['anklerotation',s2,'_',num,num2]);
save(['legmove',s2,'_',num,num2],['legmove',s2,'_',num,num2]);
save(['random',s2,'_',num,num2],['random',s2,'_',num,num2]);

Leg_60K = vertcat(wristrotateLeg_60,armrotateLeg_60,anklerotateLeg_60,legmoveLeg_60,randomLeg_60);
len = size(Leg_60K);
vec = zeros(len(1),1);
Leg_60K = [vec,Leg_60K];

for i = 1:1:length(Leg_60K)
Leg_60K(i,5)= sqrt(Leg_60K(i,3).^2+ Leg_60K(i,4).^2);
end
%--------------------------- ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_60K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_60= vq1';
%--------------- TILL Here **************************************************************************

% n=0
%  for n = 0:10:(length(Impedance) - 10)
 for i = 1:1:length(Leg_60K)
     
     
% ----------------------------ADD THIS************************************************************************** 
           if (Leg_60K(i,3)< min(aVG_60))
                Leg_60K(i,3)=Leg_60K(i,3)+400;
            end
             for s = 1:1:15                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Leg_60K(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Leg_60K(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_1(i)=  ImpedanceSequence(Index)- (a_60_1(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_1(i) =0; b_60_1(i)=0;    
              end
            end
         measurement_ohm_60K_1(i,1) = (a_60_1(i)*Leg_60K(i,3)) + b_60_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:15                                                      
        
      Condition1 = le(Leg_60K(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Leg_60K(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_1(i)=  ImpedanceSequence(Index)- (a_i_60_1(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_1(i) =0; b_i_60_1(i)=0;    
              end
            
   end
   img_60K_1(i,1) = (a_i_60_1(i)*Leg_60K(i,4)) + b_i_60_1(i)  

   if (abs(img_60K_1(i,1)-measurement_ohm_60K_1(i,1))> 0)
        
        if(abs(img_60K_1(i,1)- measurement_ohm_60K_1(i,1))<= 1600)
            measured_ohms(i,1)= measurement_ohm_60K_1(i,1);
            Leg_60K(i, 6) = measurement_ohm_60K_1(i,1);
           if(measurement_ohm_60K_1(i,1)==0)
               measured_ohms(i,1)= img_60K_1(i,1);
% <--------- CHANGE THIS VARIABLE "Impedance_Arm_60K_S006_S010" to the array where raw data to be converted is stored
               Leg_60K(i, 6) =img_60K_1(i,1); 
           end
       
        end

   end

% % Section 2
for s = 16:1:29                                                    % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Leg_60K(i,3),aVG_60(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Leg_60K(i,3),aVG_60(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_60(Index,1)-aVG_60(Index+1,1))) % calculate slope for interpolation
                    b_60_2(i)=  ImpedanceSequence(Index)- (a_60_2(i)*aVG_60(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_60_2(i) =0; b_60_2(i)=0;    
              end
            end
         measurement_ohm_60K_2(i,1) = (a_60_2(i)*Leg_60K(i,3)) + b_60_2(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 16:1:29                                                     
        
      Condition1 = le(Leg_60K(i,4),Avg_Freq_60K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Leg_60K(i,4),Avg_Freq_60K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_60_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_60K(Index,3)-Avg_Freq_60K(Index+1,3))) % calculate slope for interpolation
                    b_i_60_2(i)=  ImpedanceSequence(Index)- (a_i_60_2(i)*Avg_Freq_60K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_60_2(i) =0; b_i_60_2(i)=0;    
              end
            
   end
   img_60K_2(i,1) = (a_i_60_2(i)*Leg_60K(i,4)) + b_i_60_2(i)  

   if (abs(img_60K_2(i,1)-measurement_ohm_60K_2(i,1))> 0)
        
        if(abs(img_60K_2(i,1)- measurement_ohm_60K_2(i,1))<= 500)
            measured_ohms(i,1)= measurement_ohm_60K_2(i,1);
            Leg_60K(i, 6) = measurement_ohm_60K_2(i,1);
           if(measurement_ohm_60K_2(i,1)==0)
               measured_ohms(i,1)= img_60K_2(i,1);
               Leg_60K(i, 6) =img_60K_2(i,1);
           end
       
        end
        
   end 
  
   
 % ---------------------------------TILL HERE ***********************************************************  
 end
%  end
wristrotationLeg_60 = Leg_60K(1:20,6);
armrotationLeg_60 = Leg_60K(21:40,6);
anklerotationLeg_60 = Leg_60K(41:60,6);
legmoveLeg_60 = Leg_60K(61:80,6);
randomLeg_60 = Leg_60K(81:100,6);


save(['wristrotation',s3,'_',num,num2],['wristrotation',s3,'_',num,num2]);
save(['armrotation',s3,'_',num,num2],['armrotation',s3,'_',num,num2]);
save(['anklerotation',s3,'_',num,num2],['anklerotation',s3,'_',num,num2]);
save(['legmove',s3,'_',num,num2],['legmove',s3,'_',num,num2]);
save(['random',s3,'_',num,num2],['random',s3,'_',num,num2]);