
load('ImpedanceSequence')
load('AVG_Freq_10K_100K_30values')

num = num2str(7);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';


WB_70K = vertcat(wristrotateWB_70,armrotateWB_70,anklerotateWB_70,legmoveWB_70,randomWB_70);
len = size(WB_70K);
vec = zeros(len(1),1);
WB_70K = [vec,WB_70K];

for i = 1:1:length(WB_70K)
WB_70K(i,5)= sqrt(WB_70K(i,3).^2+ WB_70K(i,4).^2);
end

%% 70KhZ
% ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_70K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_70= vq1';
% TILL Here **************************************************************************

for i = 1:1:length(WB_70K)

% ADD THIS************************************************************************** 
 if (WB_70K(i,3)> max(aVG_70))
                WB_70K(i,3)=WB_70K(i,3)-1700;
            end      
for s = 1:1:10                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(WB_70K(i,3),aVG_70(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(WB_70K(i,3),aVG_70(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_70_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_70(Index,1)-aVG_70(Index+1,1))) % calculate slope for interpolation
                    b_70_1(i)=  ImpedanceSequence(Index)- (a_70_1(i)*aVG_70(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_70_1(i) =0; b_70_1(i)=0;    
              end
            end
         measurement_ohm_70K_1(i,1) = (a_70_1(i)*WB_70K(i,3)) + b_70_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:10                                                      
        
      Condition1 = le(WB_70K(i,4),Avg_Freq_70K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(WB_70K(i,4),Avg_Freq_70K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_70_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_70K(Index,3)-Avg_Freq_70K(Index+1,3))) % calculate slope for interpolation
                    b_i_70_1(i)=  ImpedanceSequence(Index)- (a_i_70_1(i)*Avg_Freq_70K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_70_1(i) =0; b_i_70_1(i)=0;    
              end
            
   end
   img_70K_1(i,1) = (a_i_70_1(i)*WB_70K(i,4)) + b_i_70_1(i)  

   if (abs(img_70K_1(i,1)-measurement_ohm_70K_1(i,1))> 0)
        
        if(abs(img_70K_1(i,1)- measurement_ohm_70K_1(i,1))<= 1000)
           
            measured_ohms(i,1)= measurement_ohm_70K_1(i,1);
            
            WB_70K(i, 6) = measurement_ohm_70K_1(i,1);
          
            if(measurement_ohm_70K_1(i,1)==0)
                            
                measured_ohms(i,1)= img_70K_1(i,1);
               
                WB_70K(i, 6) =img_70K_1(i,1);
           end
       
        end
        
   end
 end
% Section 2
for i = 1:1:length(WB_70K)

% ADD THIS************************************************************************** 
 if (WB_70K(i,3)> max(aVG_70))
                WB_70K(i,3)=WB_70K(i,3)-1700;
            end              
for s = 11:1:29                                                     % looking for matchpoint in the frequency file
        
              Condition1= gt(WB_70K(i,3),aVG_70(s,1))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(WB_70K(i,5),aVG_70(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_70_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_70(Index,1)-aVG_70(Index+1,1))) % calculate slope for interpolation
                    b_70_2(i)=  ImpedanceSequence(Index)- (a_70_2(i)*aVG_70(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_70_2(i) =0; b_70_2(i)=0;    
              end
             end
            measurement_ohm_70K_2(i,1) = (a_70_2(i)*WB_70K(i,3)) + b_70_2(i)
            
% Check the imaginary values to check if above point is before the 400 ohm infection        
for s = 11:1:29                                                       
        
      Condition1 = le(WB_70K(i,4),Avg_Freq_70K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(WB_70K(i,4),Avg_Freq_70K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_70_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_70K(Index,3)-Avg_Freq_70K(Index+1,3))) % calculate slope for interpolation
                    b_i_70_2(i)=  ImpedanceSequence(Index)- (a_i_70_2(i)*Avg_Freq_70K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_70_2(i) =0; b_i_70_2(i)=0;    
              end
            
   end
   img_70K_2(i,1) = (a_i_70_2(i)*WB_70K(i,4)) + b_i_70_2(i)  

   if (abs(img_70K_2(i,1)-measurement_ohm_70K_2(i,1))> 0)
        
        if(abs(img_70K_2(i,1)- measurement_ohm_70K_2(i,1))<= 1000)
            measured_ohms(i,1)= measurement_ohm_70K_2(i,1);
            WB_70K(i, 6) = measurement_ohm_70K_2(i,1);
            if(measurement_ohm_70K_2(i,1)==0)
               measured_ohms(i,1)= img_70K_2(i,1);
               WB_70K(i, 6) =img_70K_2(i,1);
           end
        end
        
        
   end
   
   
% TILL HERE ***********************************************************
end

wristrotationWB_70 = WB_70K(1:20,6);
armrotationWB_70 = WB_70K(21:40,6);
anklerotationWB_70 = WB_70K(41:60,6);
legmoveWB_70 = WB_70K(61:80,6);
randomWB_70 = WB_70K(81:100,6);

save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);

close all

Arm_70K = vertcat(wristrotateArm_70,armrotateArm_70,anklerotateArm_70,legmoveArm_70,randomArm_70);
len = size(Arm_70K);
vec = zeros(len(1),1);
Arm_70K = [vec,Arm_70K];

for i = 1:1:length(Arm_70K)
Arm_70K(i,5)= sqrt(Arm_70K(i,3).^2+ Arm_70K(i,4).^2);
end

%% 70KhZ
% ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_70K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_70= vq1';
% TILL Here **************************************************************************

for i = 1:1:length(Arm_70K)

% ADD THIS************************************************************************** 
 if (Arm_70K(i,3)> max(aVG_70))
                Arm_70K(i,3)=Arm_70K(i,3)-1700;
            end      
for s = 1:1:10                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Arm_70K(i,3),aVG_70(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Arm_70K(i,3),aVG_70(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_70_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_70(Index,1)-aVG_70(Index+1,1))) % calculate slope for interpolation
                    b_70_1(i)=  ImpedanceSequence(Index)- (a_70_1(i)*aVG_70(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_70_1(i) =0; b_70_1(i)=0;    
              end
            end
         measurement_ohm_70K_1(i,1) = (a_70_1(i)*Arm_70K(i,3)) + b_70_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:10                                                      
        
      Condition1 = le(Arm_70K(i,4),Avg_Freq_70K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Arm_70K(i,4),Avg_Freq_70K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_70_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_70K(Index,3)-Avg_Freq_70K(Index+1,3))) % calculate slope for interpolation
                    b_i_70_1(i)=  ImpedanceSequence(Index)- (a_i_70_1(i)*Avg_Freq_70K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_70_1(i) =0; b_i_70_1(i)=0;    
              end
            
   end
   img_70K_1(i,1) = (a_i_70_1(i)*Arm_70K(i,4)) + b_i_70_1(i)  

   if (abs(img_70K_1(i,1)-measurement_ohm_70K_1(i,1))> 0)
        
        if(abs(img_70K_1(i,1)- measurement_ohm_70K_1(i,1))<= 1000)
           
            measured_ohms(i,1)= measurement_ohm_70K_1(i,1);
            
            Arm_70K(i, 6) = measurement_ohm_70K_1(i,1);
          
            if(measurement_ohm_70K_1(i,1)==0)
                            
                measured_ohms(i,1)= img_70K_1(i,1);
               
                Arm_70K(i, 6) =img_70K_1(i,1);
           end
       
        end
        
   end
 end
% Section 2
for i = 1:1:length(Arm_70K)

% ADD THIS************************************************************************** 
 if (Arm_70K(i,3)> max(aVG_70))
                Arm_70K(i,3)=Arm_70K(i,3)-1700;
            end              
for s = 11:1:29                                                     % looking for matchpoint in the frequency file
        
              Condition1= gt(Arm_70K(i,3),aVG_70(s,1))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Arm_70K(i,5),aVG_70(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_70_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_70(Index,1)-aVG_70(Index+1,1))) % calculate slope for interpolation
                    b_70_2(i)=  ImpedanceSequence(Index)- (a_70_2(i)*aVG_70(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_70_2(i) =0; b_70_2(i)=0;    
              end
             end
            measurement_ohm_70K_2(i,1) = (a_70_2(i)*Arm_70K(i,3)) + b_70_2(i)
            
% Check the imaginary values to check if above point is before the 400 ohm infection        
for s = 11:1:29                                                       
        
      Condition1 = le(Arm_70K(i,4),Avg_Freq_70K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Arm_70K(i,4),Avg_Freq_70K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_70_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_70K(Index,3)-Avg_Freq_70K(Index+1,3))) % calculate slope for interpolation
                    b_i_70_2(i)=  ImpedanceSequence(Index)- (a_i_70_2(i)*Avg_Freq_70K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_70_2(i) =0; b_i_70_2(i)=0;    
              end
            
   end
   img_70K_2(i,1) = (a_i_70_2(i)*Arm_70K(i,4)) + b_i_70_2(i)  

   if (abs(img_70K_2(i,1)-measurement_ohm_70K_2(i,1))> 0)
        
        if(abs(img_70K_2(i,1)- measurement_ohm_70K_2(i,1))<= 1000)
            measured_ohms(i,1)= measurement_ohm_70K_2(i,1);
            Arm_70K(i, 6) = measurement_ohm_70K_2(i,1);
            if(measurement_ohm_70K_2(i,1)==0)
               measured_ohms(i,1)= img_70K_2(i,1);
               Arm_70K(i, 6) =img_70K_2(i,1);
           end
        end
        
        
   end
   
   
% TILL HERE ***********************************************************
end

wristrotationArm_70 = Arm_70K(1:20,6);
armrotationArm_70 = Arm_70K(21:40,6);
anklerotationArm_70 = Arm_70K(41:60,6);
legmoveArm_70 = Arm_70K(61:80,6);
randomArm_70 = Arm_70K(81:100,6);

save(['wristrotation',s2,'_',num,num2],['wristrotation',s2,'_',num,num2]);
save(['armrotation',s2,'_',num,num2],['armrotation',s2,'_',num,num2]);
save(['anklerotation',s2,'_',num,num2],['anklerotation',s2,'_',num,num2]);
save(['legmove',s2,'_',num,num2],['legmove',s2,'_',num,num2]);
save(['random',s2,'_',num,num2],['random',s2,'_',num,num2]);
close all

Leg_70K = vertcat(wristrotateLeg_70,armrotateLeg_70,anklerotateLeg_70,legmoveLeg_70,randomLeg_70);
len = size(Leg_70K);
vec = zeros(len(1),1);
Leg_70K = [vec,Leg_70K];

for i = 1:1:length(Leg_70K)
Leg_70K(i,5)= sqrt(Leg_70K(i,3).^2+ Leg_70K(i,4).^2);
end

%% 70KhZ
% ADD THIS **************************************************************************
x = 1:1:30; 
v = Avg_Freq_70K(:,2);
% Define the query points to be a finer sampling over the range of x.

xq = -1:1:30;
% EXTRAPOLATE  the function at the query points and plot the result.

figure()
% vq1 = interp1(x,v,xq);
vq1 = interp1(x,v,xq,'linear','extrap')
plot(x,v,'o',xq,vq1,':.');
xlim([-1 30]);
title('(Default) Linear EXTRApolation');

aVG_70= vq1';
% TILL Here **************************************************************************

for i = 1:1:length(Leg_70K)

% ADD THIS************************************************************************** 
 if (Leg_70K(i,3)> max(aVG_70))
                Leg_70K(i,3)=Leg_70K(i,3)-1700;
            end      
for s = 1:1:10                                                      % looking for matchpoint in the frequency file
        % cOMPARE real OF DATAFILE WITH REAL OF EXTRAPOLATED DATA IN
        % CALIBRATION FILE
              Condition1= le(Leg_70K(i,3),aVG_70(s,1))                % Comparing REAL code of data measured against averaged value in EXTRAPOLATED resistor calibration file aVG_70
              Condition2 = gt(Leg_70K(i,3),aVG_70(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_70_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_70(Index,1)-aVG_70(Index+1,1))) % calculate slope for interpolation
                    b_70_1(i)=  ImpedanceSequence(Index)- (a_70_1(i)*aVG_70(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_70_1(i) =0; b_70_1(i)=0;    
              end
            end
         measurement_ohm_70K_1(i,1) = (a_70_1(i)*Leg_70K(i,3)) + b_70_1(i) % USE REAL PART OF DATAPOINT
         
% Check the imaginary values TO CROSS CHECK      
for s = 1:1:10                                                      
        
      Condition1 = le(Leg_70K(i,4),Avg_Freq_70K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Leg_70K(i,4),Avg_Freq_70K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_70_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_70K(Index,3)-Avg_Freq_70K(Index+1,3))) % calculate slope for interpolation
                    b_i_70_1(i)=  ImpedanceSequence(Index)- (a_i_70_1(i)*Avg_Freq_70K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_70_1(i) =0; b_i_70_1(i)=0;    
              end
            
   end
   img_70K_1(i,1) = (a_i_70_1(i)*Leg_70K(i,4)) + b_i_70_1(i)  

   if (abs(img_70K_1(i,1)-measurement_ohm_70K_1(i,1))> 0)
        
        if(abs(img_70K_1(i,1)- measurement_ohm_70K_1(i,1))<= 1000)
           
            measured_ohms(i,1)= measurement_ohm_70K_1(i,1);
            
            Leg_70K(i, 6) = measurement_ohm_70K_1(i,1);
          
            if(measurement_ohm_70K_1(i,1)==0)
                            
                measured_ohms(i,1)= img_70K_1(i,1);
               
                Leg_70K(i, 6) =img_70K_1(i,1);
           end
       
        end
        
   end
 end
% Section 2
for i = 1:1:length(Leg_70K)

% ADD THIS************************************************************************** 
 if (Leg_70K(i,3)> max(aVG_70))
                Leg_70K(i,3)=Leg_70K(i,3)-1700;
            end              
for s = 11:1:29                                                     % looking for matchpoint in the frequency file
        
              Condition1= gt(Leg_70K(i,3),aVG_70(s,1))                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = le(Leg_70K(i,5),aVG_70(s+1,1))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_70_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(aVG_70(Index,1)-aVG_70(Index+1,1))) % calculate slope for interpolation
                    b_70_2(i)=  ImpedanceSequence(Index)- (a_70_2(i)*aVG_70(Index,1));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_70_2(i) =0; b_70_2(i)=0;    
              end
             end
            measurement_ohm_70K_2(i,1) = (a_70_2(i)*Leg_70K(i,3)) + b_70_2(i)
            
% Check the imaginary values to check if above point is before the 400 ohm infection        
for s = 11:1:29                                                       
        
      Condition1 = le(Leg_70K(i,4),Avg_Freq_70K(s,3))            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
      Condition2 = gt(Leg_70K(i,4),Avg_Freq_70K(s+1,3))

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_70_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_70K(Index,3)-Avg_Freq_70K(Index+1,3))) % calculate slope for interpolation
                    b_i_70_2(i)=  ImpedanceSequence(Index)- (a_i_70_2(i)*Avg_Freq_70K(Index,3));                                              % calculate intercept for interpolation
                    break;                                                        
              else a_i_70_2(i) =0; b_i_70_2(i)=0;    
              end
            
   end
   img_70K_2(i,1) = (a_i_70_2(i)*Leg_70K(i,4)) + b_i_70_2(i)  

   if (abs(img_70K_2(i,1)-measurement_ohm_70K_2(i,1))> 0)
        
        if(abs(img_70K_2(i,1)- measurement_ohm_70K_2(i,1))<= 1000)
            measured_ohms(i,1)= measurement_ohm_70K_2(i,1);
            Leg_70K(i, 6) = measurement_ohm_70K_2(i,1);
            if(measurement_ohm_70K_2(i,1)==0)
               measured_ohms(i,1)= img_70K_2(i,1);
               Leg_70K(i, 6) =img_70K_2(i,1);
           end
        end
        
        
   end
   
   
% TILL HERE ***********************************************************
end
wristrotationLeg_70 = Leg_70K(1:20,6);
armrotationLeg_70 = Leg_70K(21:40,6);
anklerotationLeg_70 = Leg_70K(41:60,6);
legmoveLeg_70 = Leg_70K(61:80,6);
randomLeg_70 = Leg_70K(81:100,6);


save(['wristrotation',s3,'_',num,num2],['wristrotation',s3,'_',num,num2]);
save(['armrotation',s3,'_',num,num2],['armrotation',s3,'_',num,num2]);
save(['anklerotation',s3,'_',num,num2],['anklerotation',s3,'_',num,num2]);
save(['legmove',s3,'_',num,num2],['legmove',s3,'_',num,num2]);
save(['random',s3,'_',num,num2],['random',s3,'_',num,num2]);
close all


