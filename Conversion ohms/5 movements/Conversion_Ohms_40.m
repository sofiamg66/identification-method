%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(4);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;
%%

x1 = ['wristrotate',s1,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s1,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s1,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s1,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s1,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

for m = 1:5
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 20KhZ to 50khz
BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2 why is n = 2?
n = 1;
for j_P = 40   % select the correct frequency file ( 10K, 20K....100K)
      FileName=[BaseName1,num2str(j_P),BaseName2];
      temp_array=eval(FileName);
      
  for i = 20*n-19:1:20*n
          for s = 1:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),temp_array(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),temp_array(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                    b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                   break;     % store the index
              else a(i) =0; b(i)=0;    
              end
        end
         measurement_ohm_20_50K_1(i,1) = (a(i)*Datafile(i,4)) + b(i) ;
         measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
        end
         n= n+1;
end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationWB_40 = ohms(:,1);
armrotationWB_40 = ohms(:,2);
anklerotationWB_40 = ohms(:,3);
legmoveWB_40 = ohms(:,4);
randomWB_40 = ohms(:,5);

save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);

x1 = ['wristrotate',s2,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s2,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s2,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s2,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s2,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];
j=0

for m = 1:5
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 20KhZ to 50khz
BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2 why is n = 2?
n = 1;
for j_P = 40   % select the correct frequency file ( 10K, 20K....100K)
      FileName=[BaseName1,num2str(j_P),BaseName2];
      temp_array=eval(FileName);
      
  for i = 20*n-19:1:20*n
          for s = 1:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),temp_array(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),temp_array(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                    b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                   break;     % store the index
              else a(i) =0; b(i)=0;    
              end
        end
         measurement_ohm_20_50K_1(i,1) = (a(i)*Datafile(i,4)) + b(i) ;
         measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
        end
         n= n+1;
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationArm_40 = ohms(:,1);
armrotationArm_40 = ohms(:,2);
anklerotationArm_40 = ohms(:,3);
legmoveArm_40 = ohms(:,4);
randomArm_40 = ohms(:,5);

save(['wristrotation',s2,'_',num,num2],['wristrotation',s2,'_',num,num2]);
save(['armrotation',s2,'_',num,num2],['armrotation',s2,'_',num,num2]);
save(['anklerotation',s2,'_',num,num2],['anklerotation',s2,'_',num,num2]);
save(['legmove',s2,'_',num,num2],['legmove',s2,'_',num,num2]);
save(['random',s2,'_',num,num2],['random',s2,'_',num,num2]);

%%
x1 = ['wristrotate',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s3,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

j = 0

for m = 1:5
Datafile = x(:,1+j*3:3+j*3);

for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 20KhZ to 50khz
BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2 why is n = 2?
n = 1;
for j_P = 40   % select the correct frequency file ( 10K, 20K....100K)
      FileName=[BaseName1,num2str(j_P),BaseName2];
      temp_array=eval(FileName);
      
  for i = 20*n-19:1:20*n
          for s = 1:1:29                                                       % looking for matchpoint in the frequency file
        
              Condition1= le(Datafile(i,4),temp_array(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
              Condition2 = gt(Datafile(i,4),temp_array(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                    b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                   break;     % store the index
              else a(i) =0; b(i)=0;    
              end
        end
         measurement_ohm_20_50K_1(i,1) = (a(i)*Datafile(i,4)) + b(i) ;
         measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
        end
         n= n+1;
end
ohms(:,m) = measured_ohms;
j = j+1;
end

wristrotationLeg_40 = ohms(:,1);
armrotationLeg_40 = ohms(:,2);
anklerotationLeg_40 = ohms(:,3);
legmoveLeg_40 = ohms(:,4);
randomLeg_40 = ohms(:,5);


save(['wristrotation',s3,'_',num,num2],['wristrotation',s3,'_',num,num2]);
save(['armrotation',s3,'_',num,num2],['armrotation',s3,'_',num,num2]);
save(['anklerotation',s3,'_',num,num2],['anklerotation',s3,'_',num,num2]);
save(['legmove',s3,'_',num,num2],['legmove',s3,'_',num,num2]);
save(['random',s3,'_',num,num2],['random',s3,'_',num,num2]);