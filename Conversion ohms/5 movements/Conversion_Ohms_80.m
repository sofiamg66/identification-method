%% Calculate in ohms

load('ImpedanceSequence.mat')
load('AVG_Freq_10K_100K_30values.mat')
clear ohms
ohms = []

num = num2str(8);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
j = 0;
%%
x1 = ['wristrotate',s1,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s1,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s1,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s1,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s1,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

    for m = 1:5
Datafile = x(:,1+j*3:3+j*3);


for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 80 kHz
for i = 1:1:20
% for i =22:1:24
 %***************************************************************************        
       for s = 1:1:15                                                       % looking for matchpoint in the frequency file
        
          Condition1= le(Datafile(i,4),Avg_Freq_80K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
          Condition2 = gt(Datafile(i,4),Avg_Freq_80K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_80_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,4)-Avg_Freq_80K(Index+1,4))); % calculate slope for interpolation
                    b_80_1(i)=  ImpedanceSequence(Index)- (a_80_1(i)*Avg_Freq_80K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_80_1(i) =0; b_80_1(i)=0;    
              end
            end
         measurement_ohm_80K_1(i,1) = (a_80_1(i)*Datafile(i,4)) + b_80_1(i); 
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:15                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_80K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_80K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_80_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,3)-Avg_Freq_80K(Index+1,3))); % calculate slope for interpolation
                    b_i_80_1(i)=  ImpedanceSequence(Index)- (a_i_80_1(i)*Avg_Freq_80K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_80_1(i) =0; b_i_80_1(i)=0;    
              end
            
   end
   img_80K_1(i,1) = (a_i_80_1(i)*Datafile(i,3)) + b_i_80_1(i);  



    if (abs(img_80K_1(i,1)-measurement_ohm_80K_1(i,1))> 0)
        
        if(abs(img_80K_1(i,1)- measurement_ohm_80K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_80K_1(i,1);
        end
        
    end 
             
         
         
 %***************************************************************************     
     for s = 16:1:29                                                       % looking for matchpoint in the frequency file
        
          Condition1= gt(Datafile(i,4),Avg_Freq_80K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
          Condition2 = le(Datafile(i,4),Avg_Freq_80K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_80_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,4)-Avg_Freq_80K(Index+1,4))); % calculate slope for interpolation
                    b_80_2(i)=  ImpedanceSequence(Index)- (a_80_2(i)*Avg_Freq_80K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
                    else a_80_2(i) =0; b_80_2(i)=0;    
              end
     end
            measurement_ohm_80K_2(i,1) = (a_80_2(i)*Datafile(i,4)) + b_80_2(i);

% Check the imaginary values to check if above point is before the 400 ohm infection        
for s = 16:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_80K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_80K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_80_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,3)-Avg_Freq_80K(Index+1,3))); % calculate slope for interpolation
                    b_i_80_2(i)=  ImpedanceSequence(Index)- (a_i_80_2(i)*Avg_Freq_80K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_80_2(i) =0; b_i_80_2(i)=0;    
              end
            
   end
   img_80K_2(i,1) = (a_i_80_2(i)*Datafile(i,3)) + b_i_80_2(i);  



    if (abs(img_80K_2(i,1)-measurement_ohm_80K_2(i,1))> 0)
        
        if(abs(img_80K_2(i,1)- measurement_ohm_80K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_80K_2(i,1);
        end
        
    end      
end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationWB_80 = ohms(:,1);
armrotationWB_80 = ohms(:,2);
anklerotationWB_80 = ohms(:,3);
legmoveWB_80 = ohms(:,4);
randomWB_80 = ohms(:,5);

save('wristrotationWB_80.mat','wristrotationWB_80');
save('armrotationWB_80','armrotationWB_80');
save('anklerotationWB_80','anklerotationWB_80');
save('legmoveWB_80','legmoveWB_80');
save('randomWB_80','randomWB_80');

x1 = ['wristrotate',s2,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s2,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s2,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s2,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s2,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];
j=0
    for m = 1:5
Datafile = x(:,1+j*3:3+j*3);


for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 80 kHz
for i = 1:1:20
% for i =22:1:24
 %***************************************************************************        
       for s = 1:1:15                                                       % looking for matchpoint in the frequency file
        
          Condition1= le(Datafile(i,4),Avg_Freq_80K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
          Condition2 = gt(Datafile(i,4),Avg_Freq_80K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_80_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,4)-Avg_Freq_80K(Index+1,4))); % calculate slope for interpolation
                    b_80_1(i)=  ImpedanceSequence(Index)- (a_80_1(i)*Avg_Freq_80K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_80_1(i) =0; b_80_1(i)=0;    
              end
            end
         measurement_ohm_80K_1(i,1) = (a_80_1(i)*Datafile(i,4)) + b_80_1(i); 
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:15                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_80K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_80K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_80_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,3)-Avg_Freq_80K(Index+1,3))); % calculate slope for interpolation
                    b_i_80_1(i)=  ImpedanceSequence(Index)- (a_i_80_1(i)*Avg_Freq_80K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_80_1(i) =0; b_i_80_1(i)=0;    
              end
            
   end
   img_80K_1(i,1) = (a_i_80_1(i)*Datafile(i,3)) + b_i_80_1(i);  



    if (abs(img_80K_1(i,1)-measurement_ohm_80K_1(i,1))> 0)
        
        if(abs(img_80K_1(i,1)- measurement_ohm_80K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_80K_1(i,1);
        end
        
    end 
             
         
         
 %***************************************************************************     
     for s = 16:1:29                                                       % looking for matchpoint in the frequency file
        
          Condition1= gt(Datafile(i,4),Avg_Freq_80K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
          Condition2 = le(Datafile(i,4),Avg_Freq_80K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_80_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,4)-Avg_Freq_80K(Index+1,4))); % calculate slope for interpolation
                    b_80_2(i)=  ImpedanceSequence(Index)- (a_80_2(i)*Avg_Freq_80K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
                    else a_80_2(i) =0; b_80_2(i)=0;    
              end
     end
            measurement_ohm_80K_2(i,1) = (a_80_2(i)*Datafile(i,4)) + b_80_2(i);

% Check the imaginary values to check if above point is before the 400 ohm infection        
for s = 16:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_80K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_80K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_80_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,3)-Avg_Freq_80K(Index+1,3))); % calculate slope for interpolation
                    b_i_80_2(i)=  ImpedanceSequence(Index)- (a_i_80_2(i)*Avg_Freq_80K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_80_2(i) =0; b_i_80_2(i)=0;    
              end
            
   end
   img_80K_2(i,1) = (a_i_80_2(i)*Datafile(i,3)) + b_i_80_2(i);  



    if (abs(img_80K_2(i,1)-measurement_ohm_80K_2(i,1))> 0)
        
        if(abs(img_80K_2(i,1)- measurement_ohm_80K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_80K_2(i,1);
        end
        
    end      
end
ohms(:,m) = measured_ohms;
j = j+1;
end
    
wristrotationArm_80 = ohms(:,1);
armrotationArm_80 = ohms(:,2);
anklerotationArm_80 = ohms(:,3);
legmoveArm_80 = ohms(:,4);
randomArm_80 = ohms(:,5);

save('wristrotationArm_80','wristrotationArm_80');
save('armrotationArm_80','armrotationArm_80');
save('anklerotationArm_80','anklerotationArm_80');
save('legmoveArm_80','legmoveArm_80');
save('randomArm_80','randomArm_80');
%%
x1 = ['wristrotate',s3,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotate',s3,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotate',s3,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s3,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s3,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

j=0
    for m = 1:5
Datafile = x(:,1+j*3:3+j*3);


for k = 1:1:20
Datafile(k,4)= sqrt(Datafile(k,2).^2 + Datafile(k,3).^2);
end
%% 80 kHz
for i = 1:1:20
% for i =22:1:24
 %***************************************************************************        
       for s = 1:1:15                                                       % looking for matchpoint in the frequency file
        
          Condition1= le(Datafile(i,4),Avg_Freq_80K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
          Condition2 = gt(Datafile(i,4),Avg_Freq_80K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_80_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,4)-Avg_Freq_80K(Index+1,4))); % calculate slope for interpolation
                    b_80_1(i)=  ImpedanceSequence(Index)- (a_80_1(i)*Avg_Freq_80K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
              else a_80_1(i) =0; b_80_1(i)=0;    
              end
            end
         measurement_ohm_80K_1(i,1) = (a_80_1(i)*Datafile(i,4)) + b_80_1(i); 
% Check the imaginary values to check if above point is before the 400 ohm infection        
 
for s = 1:1:15                                                        
        
              Condition1= le(Datafile(i,3),Avg_Freq_80K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_80K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_80_1(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,3)-Avg_Freq_80K(Index+1,3))); % calculate slope for interpolation
                    b_i_80_1(i)=  ImpedanceSequence(Index)- (a_i_80_1(i)*Avg_Freq_80K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_80_1(i) =0; b_i_80_1(i)=0;    
              end
            
   end
   img_80K_1(i,1) = (a_i_80_1(i)*Datafile(i,3)) + b_i_80_1(i);  



    if (abs(img_80K_1(i,1)-measurement_ohm_80K_1(i,1))> 0)
        
        if(abs(img_80K_1(i,1)- measurement_ohm_80K_1(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_80K_1(i,1);
        end
        
    end 
             
         
         
 %***************************************************************************     
     for s = 16:1:29                                                       % looking for matchpoint in the frequency file
        
          Condition1= gt(Datafile(i,4),Avg_Freq_80K(s,4));                % Comparing impedance code of data measured against averaged value in resistor calibration file
          Condition2 = le(Datafile(i,4),Avg_Freq_80K(s+1,4));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_80_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,4)-Avg_Freq_80K(Index+1,4))); % calculate slope for interpolation
                    b_80_2(i)=  ImpedanceSequence(Index)- (a_80_2(i)*Avg_Freq_80K(Index,4));                                              % calculate intercept for interpolation
                    break;     % store the index
                    else a_80_2(i) =0; b_80_2(i)=0;    
              end
     end
            measurement_ohm_80K_2(i,1) = (a_80_2(i)*Datafile(i,4)) + b_80_2(i);

% Check the imaginary values to check if above point is before the 400 ohm infection        
for s = 16:1:29                                                        
        
              Condition1 = le(Datafile(i,3),Avg_Freq_80K(s,3));            % Comparing imaginary code of data measured against averaged value of the real part in resistor calibration file
              Condition2 = gt(Datafile(i,3),Avg_Freq_80K(s+1,3));

              if ((Condition1 + Condition2) == 2)
                    Index = s;
                    a_i_80_2(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(Avg_Freq_80K(Index,3)-Avg_Freq_80K(Index+1,3))); % calculate slope for interpolation
                    b_i_80_2(i)=  ImpedanceSequence(Index)- (a_i_80_2(i)*Avg_Freq_80K(Index,3));                                              % calculate intercept for interpolation
                  
                    break;                                                        
              else a_i_80_2(i) =0; b_i_80_2(i)=0;    
              end
            
   end
   img_80K_2(i,1) = (a_i_80_2(i)*Datafile(i,3)) + b_i_80_2(i);  



    if (abs(img_80K_2(i,1)-measurement_ohm_80K_2(i,1))> 0)
        
        if(abs(img_80K_2(i,1)- measurement_ohm_80K_2(i,1))<= 600)
        measured_ohms(i,1)= measurement_ohm_80K_2(i,1);
        end
        
    end      
end
ohms(:,m) = measured_ohms;
j = j+1;
    end
    
    
wristrotationLeg_80 = ohms(:,1);
armrotationLeg_80 = ohms(:,2);
anklerotationLeg_80 = ohms(:,3);
legmoveLeg_80 = ohms(:,4);
randomLeg_80 = ohms(:,5);


save('wristrotationLeg_80','wristrotationLeg_80');
save('armrotationLeg_80','armrotationLeg_80');
save('anklerotationLeg_80','anklerotationLeg_80');
save('legmoveLeg_80','legmoveLeg_80')
save('randomLeg_80','randomLeg_80')
    