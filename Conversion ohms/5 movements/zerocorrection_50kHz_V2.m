%% Data Aggregate
load('ImpedanceSequence')
load('AVG_Freq_10K_100K_30values')
num = num2str(5);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';

WB_MA_50 = vertcat(wristrotateWB_50,armrotateWB_50,anklerotateWB_50,legmoveWB_50,randomWB_50);
len = size(WB_MA_50);
vec = zeros(len(1),1);
WB_MA_50 = [vec,WB_MA_50];

for i = 1:1:length(WB_MA_50)
WB_MA_50(i,5)= sqrt(WB_MA_50(i,3).^2+ WB_MA_50(i,4).^2);
end

%%  50khz

BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2

%   
for j= 50
    FileName = [BaseName1,num2str(j),BaseName2];
    temp_array=eval(FileName); 
    
    for i = 1:1:length(WB_MA_50)
               for s = 1:1:29                                                       % looking for matchpoint in the frequency file

                    Condition1= le(WB_MA_50(i,5),temp_array(s,4));              % Comparing impedance code of data measured against averaged value in resistor calibration file
                    Condition2 = gt(WB_MA_50(i,5),temp_array(s+1,4));

                    if ((Condition1 + Condition2) == 2)
                        Index = s;
                        a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                        b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                        break;     % store the index
                    else
                        a(i) =0; b(i)=0;
                    end
               end
                
               
            measurement_ohm_20_50K_1(i,1) = (a(i)*WB_MA_50(i,5)) + b(i) 
            measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
            WB_MA_50(i,6)= measurement_ohm_20_50K_1(i,1);
        
       
    end
end

wristrotationWB_50 = WB_MA_50(1:20,6);
armrotationWB_50 = WB_MA_50(21:40,6);
anklerotationWB_50 = WB_MA_50(41:60,6);
legmoveWB_50 = WB_MA_50(61:80,6);
randomWB_50 = WB_MA_50(81:100,6);

save(['wristrotation',s1,'_',num,num2],['wristrotation',s1,'_',num,num2]);
save(['armrotation',s1,'_',num,num2],['armrotation',s1,'_',num,num2]);
save(['anklerotation',s1,'_',num,num2],['anklerotation',s1,'_',num,num2]);
save(['legmove',s1,'_',num,num2],['legmove',s1,'_',num,num2]);
save(['random',s1,'_',num,num2],['random',s1,'_',num,num2]);

%%

Arm_MA_50 = vertcat(wristrotateArm_50,armrotateArm_50,anklerotateArm_50,legmoveArm_50,randomArm_50);
len = size(Arm_MA_50);
vec = zeros(len(1),1);
Arm_MA_50 = [vec,Arm_MA_50];

for i = 1:1:length(Arm_MA_50)
Arm_MA_50(i,5)= sqrt(Arm_MA_50(i,3).^2+ Arm_MA_50(i,4).^2);
end

%%  50khz

BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2

%   
for j= 50
    FileName = [BaseName1,num2str(j),BaseName2];
    temp_array=eval(FileName); 
    
    for i = 1:1:length(Arm_MA_50)
               for s = 1:1:29                                                       % looking for matchpoint in the frequency file

                    Condition1= le(Arm_MA_50(i,5),temp_array(s,4));              % Comparing impedance code of data measured against averaged value in resistor calibration file
                    Condition2 = gt(Arm_MA_50(i,5),temp_array(s+1,4));

                    if ((Condition1 + Condition2) == 2)
                        Index = s;
                        a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                        b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                        break;     % store the index
                    else
                        a(i) =0; b(i)=0;
                    end
               end
                
               
            measurement_ohm_20_50K_1(i,1) = (a(i)*Arm_MA_50(i,5)) + b(i) 
            measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
            Arm_MA_50(i,6)= measurement_ohm_20_50K_1(i,1);
        
       
    end
end

wristrotationArm_50 = Arm_MA_50(1:20,6);
armrotationArm_50 = Arm_MA_50(21:40,6);
anklerotationArm_50 = Arm_MA_50(41:60,6);
legmoveArm_50 = Arm_MA_50(61:80,6);
randomArm_50 = Arm_MA_50(81:100,6);

save(['wristrotation',s2,'_',num,num2],['wristrotation',s2,'_',num,num2]);
save(['armrotation',s2,'_',num,num2],['armrotation',s2,'_',num,num2]);
save(['anklerotation',s2,'_',num,num2],['anklerotation',s2,'_',num,num2]);
save(['legmove',s2,'_',num,num2],['legmove',s2,'_',num,num2]);
save(['random',s2,'_',num,num2],['random',s2,'_',num,num2]);

Leg_MA_50 = vertcat(wristrotateLeg_50,armrotateLeg_50,anklerotateLeg_50,legmoveLeg_50,randomLeg_50);
len = size(Leg_MA_50);
vec = zeros(len(1),1);
Leg_MA_50 = [vec,Leg_MA_50];

for i = 1:1:length(Leg_MA_50)
Leg_MA_50(i,5)= sqrt(Leg_MA_50(i,3).^2+ Leg_MA_50(i,4).^2);
end

%%  50khz

BaseName1 ='Avg_Freq_';
BaseName2 ='K';
%n =2

%   
for j= 50
    FileName = [BaseName1,num2str(j),BaseName2];
    temp_array=eval(FileName); 
    
    for i = 1:1:length(Leg_MA_50)
               for s = 1:1:29                                                       % looking for matchpoint in the frequency file

                    Condition1= le(Leg_MA_50(i,5),temp_array(s,4));              % Comparing impedance code of data measured against averaged value in resistor calibration file
                    Condition2 = gt(Leg_MA_50(i,5),temp_array(s+1,4));

                    if ((Condition1 + Condition2) == 2)
                        Index = s;
                        a(i)= ((ImpedanceSequence(Index)-ImpedanceSequence(Index+1))/(temp_array(Index,4)-temp_array(Index+1,4))); % calculate slope for interpolation
                        b(i)=  ImpedanceSequence(Index)- (a(i)*temp_array(Index,4));                                              % calculate intercept for interpolation
                        break;     % store the index
                    else
                        a(i) =0; b(i)=0;
                    end
               end
                
               
            measurement_ohm_20_50K_1(i,1) = (a(i)*Leg_MA_50(i,5)) + b(i) 
            measured_ohms(i,1)= measurement_ohm_20_50K_1(i,1);
            Leg_MA_50(i,6)= measurement_ohm_20_50K_1(i,1);
        
       
    end
end

wristrotationLeg_50 = Leg_MA_50(1:20,6);
armrotationLeg_50 = Leg_MA_50(21:40,6);
anklerotationLeg_50 = Leg_MA_50(41:60,6);
legmoveLeg_50 = Leg_MA_50(61:80,6);
randomLeg_50 = Leg_MA_50(81:100,6);

save(['wristrotation',s3,'_',num,num2],['wristrotation',s3,'_',num,num2]);
save(['armrotation',s3,'_',num,num2],['armrotation',s3,'_',num,num2]);
save(['anklerotation',s3,'_',num,num2],['anklerotation',s3,'_',num,num2]);
save(['legmove',s3,'_',num,num2],['legmove',s3,'_',num,num2]);
save(['random',s3,'_',num,num2],['random',s3,'_',num,num2]);


