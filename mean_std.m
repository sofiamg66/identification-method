% Set up for 10 data points in movement!

% Caculation of: mean for still supine and motion artefact; percentage
% change between means for still supine and motion artefact; standard
% deviation for still supine and motion artefact; percentage change between
% standard deviations for still supine and motion artefact

name1 = 'wrist rotation';
name2 = 'arm rotation';
name3 = 'ankle rotation';
name4 = 'leg flextion/extension';
name5 = 'roll from side to side';
names = {name1;name2;name3;name4;name5};
names2 = {'Wrist rotation','Arm rotation','Ankle rotation','Leg flexion/extension','Roll side to side'};

std_supine = []; 
std_movement = [];
change_means = [];
change_std = []; 
mean_supine = [];
mean_movement = [];
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
s = s1;
%%
for freq = 1:9
num = num2str(freq);

x1 = ['wristrotation',s,'_',num,num2];
x_1 = eval(x1);
x2 = ['armrotation',s,'_',num,num2];
x_2 = eval(x2);
x3 = ['anklerotation',s,'_',num,num2];
x_3 = eval(x3);
x4 = ['legmove',s,'_',num,num2];
x_4 = eval(x4);
x5 = ['random',s,'_',num,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

% To use with the data from subject 2, since the data was recorded
% separately, you need to comment the x variable above
%x1m = ['wristrotation',s,'_',num,num2,'_2'];
%x_1m = eval(x1m);
%x2m = ['armrotation',s,'_',num,num2,'_2'];
%x_2m = eval(x2m);
%x3m = ['anklerotation',s,'_',num,num2,'_2'];
%x_3m = eval(x3m);
%x4m = ['legmove',s,'_',num,num2,'_2'];
%x_4m = eval(x4m);
%x5m = ['random',s,'_',num,num2,'_2'];
%x_5m = eval(x5m);

%x_1s = [x_1;x_1m];
%x_2s = [x_2;x_2m];
%x_3s = [x_3;x_3m];
%x_4s = [x_4;x_4m];
%x_5s = [x_5;x_5m];
%x = [x_1s x_2s x_3s x_4s x_5s];

% change length mean and std
mean_sum = [0,0,0,0,0];
mean_summ = [0,0,0,0,0];
supine = [];
movement = [];
movement_boxplot = [];

for i = 1:5
    i_m = 0;
    i_s = 0;
%stup length of motion artefact and supine
std_sum_sup = [];
std_sum_move = [];
supine(:,i) = x(21:30,i);
movement(:,i) = x(31:40,i);
std_sum_sup(:,1) = x(21:30,i);
std_sum_move(:,1) = x(31:40,i);

length_supine = length(supine(:,i));
length_movement = length(movement(:,i));
for k = 1:length_supine
    if supine(k,i) ~= 0
        mean_sum(1,i) = mean_sum(1,i)+supine(k,i);
        i_s = i_s+1;
        
    else
        disp('There is a zero in supine in ')
        freq
        k
        std_sum_sup(k,1) = nan;
    end
end
mean_supine(freq,i) = mean_sum(1,i)/i_s;

for k = 1:length_movement
    if movement(k,i) ~= 0
        mean_summ(1,i) = mean_summ(1,i)+movement(k,i);
        i_m = i_m+1; 
    else
        disp('There is a zero in movement in ')
        freq
        k
        std_sum_move(k,1) = nan;
    end
end
mean_movement(freq,i) = mean_summ(1,i)/i_m;


std_supine(freq,i) = std(std_sum_sup(:,1),'omitnan');
std_movement(freq,i) = std(std_sum_move(:,1),'omitnan');

change_means(freq,i) = (mean_movement(freq,i)-mean_supine(freq,i))/mean_supine(freq,i)*100;
change_std(freq,i) = (std_movement(freq,i)-std_supine(freq,i))/std_supine(freq,i)*100;


end
if freq ==9
box_x = x(31:40,:);
supine_box = x(21:30,1);
figure(freq)
%boxplot([supine_box,box_x],'Labels',{['Supine std = ' num2str(std_supine(freq,1)),'; mean = ' num2str(mean_supine(freq,1))],...
    %['Wrist std = ' num2str(std_movement(freq,1)),'; mean = ' num2str(mean_movement(freq,1))],['Arm std = ' num2str(std_movement(freq,2))...
   % ,'; mean = ' num2str(mean_movement(freq,2))],['Ankle std = ' num2str(std_movement(freq,3)),'; mean = ' num2str(mean_movement(freq,3))],['Leg std = ' num2str(std_movement(freq,4)),'; mean = ' num2str(mean_movement(freq,4))],['Random std = ' num2str(std_movement(freq,5)),'; mean = ' num2str(mean_movement(freq,5))]});

 boxplot([supine_box,box_x],'Labels',{['Supine'],['Wrist'],['Arm'],['Ankle'],['Leg'],['Roll side to side']});
    %['Wrist std = ' num2str(std_movement(freq,1)),'; mean = ' num2str(mean_movement(freq,1))],['Arm std = ' num2str(std_movement(freq,2))...
   % ,'; mean = ' num2str(mean_movement(freq,2))],['Ankle std = ' num2str(std_movement(freq,3)),'; mean = ' num2str(mean_movement(freq,3))],['Leg std = ' num2str(std_movement(freq,4)),'; mean = ' num2str(mean_movement(freq,4))],['Random std = ' num2str(std_movement(freq,5)),'; mean = ' num2str(mean_movement(freq,5))]});
xtickangle(45)
ylabel('Impedance Value (ohms)','FontSize',14)
h = findobj(gca, 'type', 'text');
set(h, 'FontSize', 14);
hold on
title(['Box plot with leg segment bio-impedance measurements at 90kHz'],'FontSize',14)
%title([sprintf('%s',names{i})]) 
%arreglar savefig, si no manually
%saveas(gcf,sprintf('boxplot_WB_%s.png',names{i}))
savefig(['boxplot_Leg_',freq,'0.fig'])
else 
end

separator = zeros(1,5);
% automatically save in an excel
tablenames = table(['mean_supine';'mean_movemt';'percen_mean';'___________';'std__supine';'std__movemt';'percen__std']);
dataprint = [mean_supine(freq,:);mean_movement(freq,:);change_means(freq,:);separator;std_supine(freq,:);std_movement(freq,:);change_std(freq,:)];
data_temp = array2table(dataprint);
%data_temp.Properties.VariableNames = {'wristrotate' 'wristupdown' 'armrotate' 'armupdown' 'anklerotate' 'ankleupdown' 'legmove' 'random'};
data = [tablenames,data_temp];
%writetable(data,['mean_std_',s,'_freq',num,'0.xls']);

end
%%
num = num2str(1);
x1 = ['wristrotation',s,'_',num,num2,num2];
x_1 = eval(x1);
x2 = ['armrotation',s,'_',num,num2,num2];
x_2 = eval(x2);
x3 = ['anklerotation',s,'_',num,num2,num2];
x_3 = eval(x3);
x4 = ['legmove',s,'_',num,num2,num2];
x_4 = eval(x4);
x5 = ['random',s,'_',num,num2,num2];
x_5 = eval(x5);
x = [x_1 x_2 x_3 x_4 x_5];

% To use with the data from subject 2, since the data was recorded
% separately, you need to comment the x variable above

%x1m = ['wristrotation',s,'_',num,num2,'_2'];
%x_1m = eval(x1m);
%x2m = ['armrotation',s,'_',num,num2,'_2'];
%x_2m = eval(x2m);
%x3m = ['anklerotation',s,'_',num,num2,'_2'];
%x_3m = eval(x3m);
%x4m = ['legmove',s,'_',num,num2,'_2'];
%x_4m = eval(x4m);
%x5m = ['random',s,'_',num,num2,'_2'];
%x_5m = eval(x5m);

%x_1s = [x_1;x_1m];
%x_2s = [x_2;x_2m];
%x_3s = [x_3;x_3m];
%x_4s = [x_4;x_4m];
%x_5s = [x_5;x_5m];
%x = [x_1s x_2s x_3s x_4s x_5s];

supine = [];
movement = [];
movement_boxplot = [];
mean_sum = [0,0,0,0,0];
mean_summ = [0,0,0,0,0];


for i = 1:5
    i_m = 0;
    i_s = 0;
    
std_sum_sup = [];
std_sum_move = [];

supine(:,i) = x(21:30,i);
movement(:,i) = x(31:40,i);
std_sum_sup(:,1) = x(21:30,i);
std_sum_move(:,1) = x(31:40,i);

length_supine = length(supine(:,i));
length_movement = length(movement(:,i));

for k = 1:length_supine
    if supine(k,i) ~= 0
        mean_sum(1,i) = mean_sum(1,i)+supine(k,i);
        i_s = i_s+1;
    else
        disp('There is a zero in supine in ')
        freq
        k
        std_sum_sup(k,1) = nan;
    end
end
mean_supine(10,i) = mean_sum(1,i)/i_s;
for k = 1:length_movement
    if movement(k,i) ~= 0
        mean_summ(1,i) = mean_summ(1,i)+movement(k,i);
        i_m = i_m+1; 
    else
        disp('There is a zero in supine in ')
        freq
        k
        std_sum_move(k,1) = nan;
    end
end
mean_movement(10,i) = mean_summ(1,i)/i_m;

std_supine(10,i) = std(std_sum_sup(:,1),'omitnan');
std_movement(10,i) = std(std_sum_move(:,1),'omitnan');

change_means(10,i) = (mean_movement(10,i)-mean_supine(10,i))/mean_supine(10,i)*100;
change_std(10,i) = (std_movement(10,i)-std_supine(10,i))/std_supine(10,i)*100;



end
freq = 10;
box_x = x(31:40,:);
supine_box = x(21:30,1);
figure(10)
boxplot([supine_box,box_x],'Labels',{['Supine std = ' num2str(std_supine(freq,1)),'; mean = ' num2str(mean_supine(freq,1))],...
    ['Wrist std = ' num2str(std_movement(freq,1)),'; mean = ' num2str(mean_movement(freq,1))],['Arm std = ' num2str(std_movement(freq,2))...
    ,'; mean = ' num2str(mean_movement(freq,2))],['Ankle std = ' num2str(std_movement(freq,3)),'; mean = ' num2str(mean_movement(freq,3))],['Leg std = ' num2str(std_movement(freq,4)),'; mean = ' num2str(mean_movement(freq,4))],['Random std = ' num2str(std_movement(freq,5)),'; mean = ' num2str(mean_movement(freq,5))]});
xtickangle(45)

ylabel('Impedance Value (ohms)','FontSize',10)
hold on
title(['Box plot with whole body segment bio-impedance for all movements at 100kHz'])
savefig(['boxplot_WB_100.fig'])


change_std_s1 = change_std(:,:);
%save('WBchange_std_s1','change_std_s1');
change_means_s1 = change_means(:,:);
%save('WBchange_means_s1','change_means_s1');
close all
separator = zeros(1,5);
% automatically save in an excel
tablenames = table(['mean_supine';'mean_movemt';'percen_mean';'___________';'std__supine';'std__movemt';'percen__std']);
dataprint = [mean_supine(10,:);mean_movement(10,:);change_means(10,:);separator;std_supine(10,:);std_movement(10,:);change_std(10,:)];
data_temp = array2table(dataprint);
%data_temp.Properties.VariableNames = {'wristrotate' 'wristupdown' 'armrotate' 'armupdown' 'anklerotate' 'ankleupdown' 'legmove' 'random'};
data = [tablenames,data_temp];
%writetable(data,['mean_std_',s,'_freq',num,'00.xls']);


%for i =1:5
%for i =1:9 % loop for the frequencies
%figure(i)
%boxplot(movement(:,i,:),'Labels',{['10kHz std = ' num2str(std_movement(1,i));'mean = ' num2str(mean_movement(1,i))],['20kHz std = ' num2str(std_movement(2,i));'mean = ' num2str(mean_movement(2,i))],['30kHz std = ' num2str(std_movement(3,i));'mean = ' num2str(mean_movement(3,i))],...
 %   ['40kHz std = ' num2str(std_movement(4,i));'mean = ' num2str(mean_movement(4,i))],['50kHz std = ' num2str(std_movement(5,i));'mean = ' num2str(mean_movement(5,i))],['60kHz std = ' num2str(std_movement(6,i));'mean = ' num2str(mean_movement(6,i))],...
 %   ['70kHz std = ' num2str(std_movement(7,i));'mean = ' num2str(mean_movement(7,i))],['80kHz std = ' num2str(std_movement(8,i));'mean = ' num2str(mean_movement(8,i))],['90kHz std = ' num2str(std_movement(9,i));'mean = ' num2str(mean_movement(9,i))],['100kHz std = ' num2str(std_movement(10,i));'mean = ' num2str(mean_movement(10,i))]});  
%xtickangle(45)
%hold on
%title(['Box plot with whole body segment bio-impedance at all frequencies, movement ',sprintf('%s',names{i})])
%arreglar title 
%savefig(['boxplot_',sprintf('%s',names2{i}),'_WB.fig'])
%arreglar savefig, si no manually
%saveas(gcf,sprintf('boxplot_WB_%s.png',names{i}))
%filename =sprintf('boxplot_WB_%s.png',names2{i});
%print(gcf,filename,'-depsc')
%end

function notused(freq)
for i = 1:5
figure(i)
for freq = 1:10
err2 = abs(change_std(freq,i))
e5 =errorbar(points(freq),change_means(freq,i),err2)
e5.Marker = 'o';
e5.MarkerSize = 3;
hold on 
end
%xticklabels({['At 10kHz, %change= ' num2str(change_means(1,i))],['At 20kHz, %change= ' num2str(change_means(2,i))],['At 30kHz, %change= ' num2str(change_means(3,i))],['At 40kHz, %change= ' num2str(change_means(4,i))],['At 50kHz, %change= ' num2str(change_means(5,i))],['At 60kHz, %change= ' num2str(change_means(6,i))],['At 70kHz, %change= ' num2str(change_means(7,i))],['At 80kHz, %change= ' num2str(change_means(8,i))],['At 90kHz, %change= ' num2str(change_means(9,i))],['At 100kHz, %change= ' num2str(change_means(10,i))]})
xticks([1 2 3 4 5 6 7 8 9 10])
xticklabels({['10kHz'],['20kHz'],['30kHz'],['40kHz'],['50kHz'],['60kHz'],['70kHz'],['80kHz'],['90kHz'],['100kHz']})
xtickangle(45)
set(gca, 'YAxisLocation', 'origin')
%YL = get(gca, 'YLimit');
maxlim = max(abs(change_std(:,i)));
ylim([-maxlim maxlim]);
xlim([0 11])
ylabel('Percentage change means','FontSize',10)
title(['Whole body segment supine still and ',sprintf('%s',names2{i}),' means and standard deviation percentage change'],'FontSize',10)
%set(get(gca,'title'),'Position',[5.5 10 0])
savefig(['change_',sprintf('%s',names{i}),'_',s,'.fig'])
end
saveas(figure(1),'change_wristrotation.jpg')
saveas(figure(2),'change_armrotation.jpg')
saveas(figure(3),'change_anklerotation.jpg')
saveas(figure(4),'change_legmove.jpg')
saveas(figure(5),'change_random.jpg')
close all
end


