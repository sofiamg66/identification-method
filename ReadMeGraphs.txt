This file provides instructions on how to use the graphics software on matlab that was used to analyse impedance data for motion artefact charcaterisation.

1. 'Allgraphs.m' script contains the code to plot all the graphs used for analysis. The corresponding function need to be selected to plot different figures, following the changing the code inside 'Allgraphs.m'.

   If the user wants to analyse the data from subject 1, he needs to change some part of the code as instructed. The user has to load the collected data from the following folder: Data > Data1 > data1_ohms_converted > allWB/allArm/allLeg, depending on the segment to be analysed.  

   If the user wants to analyse the data from subject 2, he needs to change some part of the code as instructed. The user has to load the collected data from the following folder: Data > Data2 > data2_ohms_converted > allvariables.  

2. 'box4move.m' script contains the code to plot a box plot with supine reference and following four movements from subject 2: wrist rotation, arm rotation, ankle rotation, leg flexion/extension. 
   If the user wants to analyse the data from subject 2, he needs to change some part of the code as instructed. The user has to load the collected data from the following folder: Data > Data2 > data2_ohms_converted > allvariables.  