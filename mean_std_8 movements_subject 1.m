% Set up for 10 data points in movement!

% Caculation of: mean for still supine and motion artefact; percentage
% change between means for still supine and motion artefact; standard
% deviation for still supine and motion artefact; percentage change between
% standard deviations for still supine and motion artefact

name1 = 'wristrotation';
name2 = 'wristupdown';
name3 = 'armrotation';
name4 = 'armupdown';
name5 = 'anklerotation';
name6 = 'ankleupdown';
name7 = 'legmove';
name8 = 'random';
names = {name1;name2;name3;name4;name5;name6;name7;name8};
std_supine = []; 
std_movement = [];
change_means = [];
change_std = []; 
mean_supine = [];
mean_movement = [];

for freq = 1:9
num = num2str(freq);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
s = s1;

x1 = ['wristrotation',s,'_',num,num2];
x_1 = eval(x1);
x2 = ['wristupdown',s,'_',num,num2];
x_2 = eval(x2);
x3 = ['armrotation',s,'_',num,num2];
x_3 = eval(x3);
x4 = ['armupdown',s,'_',num,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s,'_',num,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s,'_',num,num2];
x_6 = eval(x6);
x7 = ['legmove',s,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s,'_',num,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

supine = [];
movement = [];
% change length mean and std
mean_sum = [0,0,0,0,0,0,0,0];
mean_summ = [0,0,0,0,0,0,0,0];

for i = 1:8
    i_m = 0;
    i_s = 0;
%stup length of motion artefact and supine
std_sum_sup = [];
std_sum_move = [];
supine(:,i) = x(21:30,i);
movement(:,i) = x(31:end,i);
std_sum_sup(:,1) = x(21:30,i);
std_sum_move(:,1) = x(31:end,i);

length_supine = length(supine(:,i));
length_movement = length(movement(:,i));
for k = 1:length_supine
    if supine(k,i) ~= 0
        mean_sum(1,i) = mean_sum(1,i)+supine(k,i);
        i_s = i_s+1;
        
    else
        disp('There is a zero in supine in ')
        freq
        k
        std_sum_sup(k,1) = nan;
    end
end
mean_supine(freq,i) = mean_sum(1,i)/i_s;

for k = 1:length_movement
    if movement(k,i) ~= 0
        mean_summ(1,i) = mean_summ(1,i)+movement(k,i);
        i_m = i_m+1; 
    else
        disp('There is a zero in movement in ')
        freq
        k
        std_sum_move(k,1) = nan;
    end
end
mean_movement(freq,i) = mean_summ(1,i)/i_m;


std_supine(freq,i) = std(std_sum_sup(:,1),'omitnan');
std_movement(freq,i) = std(std_sum_move(:,1),'omitnan');

change_means(freq,i) = (mean_movement(freq,i)-mean_supine(freq,i))/mean_supine(freq,i)*100;
change_std(freq,i) = (std_movement(freq,i)-std_supine(freq,i))/std_supine(freq,i)*100;
end

%mean_supine(freq,:) = [mean_supine(freq,:);mean_supine_prov];
%mean_movement(freq,:) = [mean_movement(freq,:);mean_movement_prov];

separator = zeros(1,8);
% automatically save in an excel
tablenames = table(['mean_supine';'mean_movemt';'percen_mean';'___________';'std__supine';'std__movemt';'percen__std']);
dataprint = [mean_supine(freq,:);mean_movement(freq,:);change_means(freq,:);separator;std_supine(freq,:);std_movement(freq,:);change_std(freq,:)];
data_temp = array2table(dataprint);
data_temp.Properties.VariableNames = {'wristrotate' 'wristupdown' 'armrotate' 'armupdown' 'anklerotate' 'ankleupdown' 'legmove' 'random'};
data = [tablenames,data_temp];
writetable(data,['mean_std_',s,'_freq',num,'0.xls']);
end

num = num2str(1);
x1 = ['wristrotation',s,'_',num,num2,num2];
x_1 = eval(x1);
x2 = ['wristupdown',s,'_',num,num2,num2];
x_2 = eval(x2);
x3 = ['armrotation',s,'_',num,num2,num2];
x_3 = eval(x3);
x4 = ['armupdown',s,'_',num,num2,num2];
x_4 = eval(x4);
x5 = ['anklerotation',s,'_',num,num2,num2];
x_5 = eval(x5);
x6 = ['ankleupdown',s,'_',num,num2,num2];
x_6 = eval(x6);
x7 = ['legmove',s,'_',num,num2,num2];
x_7 = eval(x7);
x8 = ['random',s,'_',num,num2,num2];
x_8 = eval(x8);
x = [x_1 x_2 x_3 x_4 x_5 x_6 x_7 x_8];

supine = [];
movement = [];
mean_sum = [0,0,0,0,0,0,0,0];
mean_summ = [0,0,0,0,0,0,0,0];


for i = 1:8
    i_m = 0;
    i_s = 0;
    
std_sum_sup = [];
std_sum_move = [];

supine(:,i) = x(21:30,i);
movement(:,i) = x(31:end,i);
std_sum_sup(:,1) = x(21:30,i);
std_sum_move(:,1) = x(31:end,i);

length_supine = length(supine(:,i));
length_movement = length(movement(:,i));

for k = 1:length_supine
    if supine(k,i) ~= 0
        mean_sum(1,i) = mean_sum(1,i)+supine(k,i);
        i_s = i_s+1;
    else
        disp('There is a zero in supine in ')
        freq
        k
        std_sum_sup(k,1) = nan;
    end
end
mean_supine(10,i) = mean_sum(1,i)/i_s;
for k = 1:length_movement
    if movement(k,i) ~= 0
        mean_summ(1,i) = mean_summ(1,i)+movement(k,i);
        i_m = i_m+1; 
    else
        disp('There is a zero in supine in ')
        freq
        k
        std_sum_move(k,1) = nan;
    end
end
mean_movement(10,i) = mean_summ(1,i)/i_m;

std_supine(10,i) = std(std_sum_sup(:,1),'omitnan');
std_movement(10,i) = std(std_sum_move(:,1),'omitnan');

change_means(10,i) = (mean_movement(10,i)-mean_supine(10,i))/mean_supine(10,i)*100;
change_std(10,i) = (std_movement(10,i)-std_supine(10,i))/std_supine(10,i)*100;
end
separator = zeros(1,8);
% automatically save in an excel
tablenames = table(['mean_supine';'mean_movemt';'percen_mean';'___________';'std__supine';'std__movemt';'percen__std']);
dataprint = [mean_supine(10,:);mean_movement(10,:);change_means(10,:);separator;std_supine(10,:);std_movement(10,:);change_std(10,:)];
data_temp = array2table(dataprint);
data_temp.Properties.VariableNames = {'wristrotate' 'wristupdown' 'armrotate' 'armupdown' 'anklerotate' 'ankleupdown' 'legmove' 'random'};
data = [tablenames,data_temp];
writetable(data,['mean_std_',s,'_freq',num,'00.xls']);



%graph comparing frequencies for one movement
points = [1 2 3 4 5 6 7 8 9 10];
for i = 1:8
    subplot(i)
for freq = 1:10
err2 = change_std(freq,i)
e5 =errorbar(points(freq),change_means(freq,i),err2)
e5.Marker = 'o';
e5.MarkerSize = 3;
hold on 
end
%xticklabels({['At 10kHz, %change= ' num2str(change_means(1,i))],['At 20kHz, %change= ' num2str(change_means(2,i))],['At 30kHz, %change= ' num2str(change_means(3,i))],['At 40kHz, %change= ' num2str(change_means(4,i))],['At 50kHz, %change= ' num2str(change_means(5,i))],['At 60kHz, %change= ' num2str(change_means(6,i))],['At 70kHz, %change= ' num2str(change_means(7,i))],['At 80kHz, %change= ' num2str(change_means(8,i))],['At 90kHz, %change= ' num2str(change_means(9,i))],['At 100kHz, %change= ' num2str(change_means(10,i))]})
xticklabels({['10kHz'],['20kHz'],['30kHz'],['40kHz'],['50kHz'],['60kHz'],['70kHz'],['80kHz'],['90kHz'],['100kHz']})
xtickangle(45)
set(gca, 'YAxisLocation', 'origin')
%YL = get(gca, 'YLimit');
maxlim = max(abs(change_std(:,i)));
ylim([-maxlim maxlim]);
ylabel('Percentage change means','FontSize',10)
title(['Whole body segment supine still and ',sprintf('%s',names{i}),' means and standard deviation percentage change']'FontSize',14)
%savefig(['means_',sprintf('%s',names{i}),'_',s,'.fig'])
end
saveas(gcf,'WB_percentagechanges.jpg')
%%
% graph comparing segments for one movement one frequency
