%% Graphs
%Different functions to visualize the recorded data
%Change the title in case of using other body segment
%Change names to save the plots

% Vector to plot the data against the timestamps
datapoints = linspace(1.2,72,60);

% Names of the analysed movements 
name1 = 'Wrist rotation';
name2 = 'Wrist flexion/extension';
name3 = 'Arm rotation';
name4 = 'Arm flexion/extension';
name5 = 'Ankle rotation';
name6 = 'Ankle flexion/extension';
name7 = 'Leg flextion/extension';
name8 = 'Roll from side to side';
% Select if 5 movements or 8 movements
%names = {name1;name2;name3;name4;name5;name6;name7;name8};
names = {name1;name3;name5;name7;name8};

% select the frequency and the segment to plot
num = num2str(9);
num2 = num2str(0);
s1 = 'WB';
s2 = 'Arm';
s3 = 'Leg';
s = s3;

%% Eight movements 
x1 = ['wristrotation',s,'_',num,num2];
x_1 = eval(x1);
%x2 = ['wristupdown',s,'_',num,num2];
%x_2 = eval(x2);
x3 = ['armrotation',s,'_',num,num2];
x_3 = eval(x3);
%x4 = ['armupdown',s,'_',num,num2];
%x_4 = eval(x4);
x5 = ['anklerotation',s,'_',num,num2];
x_5 = eval(x5);
%x6 = ['ankleupdown',s,'_',num,num2];
%x_6 = eval(x6);
x7 = ['legmove',s,'_',num,num2];
x_7 = eval(x7);
x8 = ['random',s,'_',num,num2];
x_8 = eval(x8);
%% 5 movements
% Uncoment this from 48-57 if the user wants to create the plots for
% subject 2, frequencies 10kHz to 90kHz

%x1m = ['wristrotation',s,'_',num,num2,'_2'];
%x_1m = eval(x1m);
%x2m = ['armrotation',s,'_',num,num2,'_2'];
%x_2m = eval(x2m);
%x3m = ['anklerotation',s,'_',num,num2,'_2'];
%x_3m = eval(x3m);
%x4m = ['legmove',s,'_',num,num2,'_2'];
%x_4m = eval(x4m);
%x5m = ['random',s,'_',num,num2,'_2'];
%x_5m = eval(x5m);
%% 100kHz
%
%x1 = ['wristrotation',s,'_',num,num2,num2];
%x_1 = eval(x1);
%x2 = ['wristupdown',s,'_',num,num2,num2];
%x_2 = eval(x2);
%x3 = ['armrotation',s,'_',num,num2,num2];
%x_3 = eval(x3);
%x4 = ['armupdown',s,'_',num,num2,num2];
%x_4 = eval(x4);
%x5 = ['anklerotation',s,'_',num,num2,num2];
%x_5 = eval(x5);
%x6 = ['ankleupdown',s,'_',num,num2,num2];
%x_6 = eval(x6);
%x7 = ['legmove',s,'_',num,num2,num2];
%x_7 = eval(x7);
%x8 = ['random',s,'_',num,num2,num2];
%x_8 = eval(x8);

% Uncoment this from 83-92 if the user wants to create the plots for
% subject 2, frequency 100 kHz

%x1m = ['wristrotation',s,'_',num,num2,num2,'_2'];
%x_1m = eval(x1m);
%x2m = ['armrotation',s,'_',num,num2,num2,'_2'];
%x_2m = eval(x2m);
%x3m = ['anklerotation',s,'_',num,num2,num2,'_2'];
%x_3m = eval(x3m);
%x4m = ['legmove',s,'_',num,num2,num2,'_2'];
%x_4m = eval(x4m);
%x5m = ['random',s,'_',num,num2,num2,'_2'];
%x_5m = eval(x5m);
 
% Uncomment from 96-102 and comment line 92 if the user wants to create
% plots for subject 2
%x_1s = [x_1;x_1m];
%x_2s = [x_3;x_2m];
%x_3s = [x_5;x_3m];
%x_4s = [x_7;x_4m];
%x_5s = [x_8;x_5m];

%x = [x_1s x_2s x_3s x_4s x_5s];

% x variable is set for 5 movements, if the user wants to analyze 8
% movements, he needs to adjust this variable with the corresponding
% movements

x = [x_1,x_3,x_5,x_7,x_8];

% In case the user wants to plot the comparison between rotation and up and down
% in a subplot

%x3 = ['anklerotation',s,'_',num,num2];
%x_3 = eval(x3);
%x4 = ['ankleupdown',s,'_',num,num2];
%x_4 = eval(x4);
%s = s3;
%x3 = ['anklerotation',s,'_',num,num2];
%x_5 = eval(x3);
%x4 = ['ankleupdown',s,'_',num,num2];
%x_6 = eval(x4);

%Plot all measurements
allplot(x,datapoints',names)

%Plot at one frequency
%onefreq(armrotationLeg_90,datapoints)

%Plot with supine position with a motion artefact (no mean)
%Change legends
%compare2(x,x_2,x_3,x_4,x_5,x_6,datapoints',s,num);

% Boxplots, this box function has been not used for the report. Boxplots
% for the report have been created from the mean_std.m file code
%deviation = box(supinerandom)

close all
function onefreq(movement,datapoints)
    figure(1)
    plot(datapoints(1:31,1),movement(1:31,1),'b');
    xlabel ('N = 60 Data Points', 'FontSize',8,'FontWeight','bold')
    ylabel ('Impedance Value (ohms)', 'FontSize',8,'FontWeight','bold')
    title('Leg supine arm rotation bio-impedance at 90kHz')
    hold on
    plot(datapoints(31:end,1),movement(31:end,1),'r');
    legend('Supine','Wrist rotation')
   % savefig('freq100_supinerandom_WB.fig')
   saveas(gcf,'freq90_supinerandom_Leg.jpg')
  %  close all
end
function compare2(movement1,movement2,movement3,movement4,movement5,movement6,datapoints,s,num)
subplot(3,1,1)
plot(datapoints(1:10,1),movement1(21:30,1),'k','HandleVisibility','off')
hold on
plot(datapoints(1:10,1),movement1(21:30,1),'ok','HandleVisibility','off')
xlabel ('Time (s)', 'FontSize',10)
ylabel ('Impedance Value (ohms)', 'FontSize',10)
title(['Whole body segment ankle movement bio-impedance subject 2 at 90kHz'],'FontSize',12,'FontWeight','bold')
plot(datapoints(1:10,1),movement2(21:30,1),'k','HandleVisibility','off')
plot(datapoints(11:20,1),movement1(31:40,1),'b','HandleVisibility','off')  
plot(datapoints(11:20,1),movement2(31:40,1),'g','HandleVisibility','off')
plot(datapoints(1:10,1),movement2(21:30,1),'ok')
plot(datapoints(11:20,1),movement1(31:40,1),'ob')  
plot(datapoints(11:20,1),movement2(31:40,1),'og')
%ylim([400 850])
lh = legend('Reference-supine','Rotation','Flexion/extension','Location','east')
set(lh,'position',[0.85 0.8 .1 .1])
subplot(3,1,2)
plot(datapoints(1:10,1),movement3(21:30,1),'k','HandleVisibility','off')
hold on
plot(datapoints(1:10,1),movement3(21:30,1),'ok','HandleVisibility','off')
xlabel ('Time (s)', 'FontSize',10)
ylabel ('Impedance Value (ohms)', 'FontSize',10)
title(['Arm segment ankle movement bio-impedance subject 2 at 90kHz'],'FontSize',12,'FontWeight','bold')
plot(datapoints(1:10,1),movement4(21:30,1),'k','HandleVisibility','off')
plot(datapoints(11:20,1),movement3(31:40,1),'b','HandleVisibility','off')  
plot(datapoints(11:20,1),movement4(31:40,1),'g','HandleVisibility','off') 
plot(datapoints(1:10,1),movement4(21:30,1),'ok','HandleVisibility','off')
plot(datapoints(11:20,1),movement3(31:40,1),'ob','HandleVisibility','off')  
plot(datapoints(11:20,1),movement4(31:40,1),'og','HandleVisibility','off') 

subplot(3,1,3)
plot(datapoints(1:10,1),movement5(21:30,1),'k','HandleVisibility','off')
hold on
plot(datapoints(1:10,1),movement5(21:30,1),'ok','HandleVisibility','off')   
xlabel ('Time (s)', 'FontSize',10)
ylabel ('Impedance Value (ohms)', 'FontSize',10)
title(['Leg segment ankle movement bio-impedance subject 2 at 90kHz'],'FontSize',12,'FontWeight','bold')
plot(datapoints(1:10,1),movement6(21:30,1),'k','HandleVisibility','off')
plot(datapoints(11:20,1),movement5(31:40,1),'b','HandleVisibility','off')  
plot(datapoints(11:20,1),movement6(31:40,1),'g','HandleVisibility','off') 
plot(datapoints(1:10,1),movement6(21:30,1),'ok','HandleVisibility','off')
plot(datapoints(11:20,1),movement5(31:40,1),'ob','HandleVisibility','off')  
plot(datapoints(11:20,1),movement6(31:40,1),'og','HandleVisibility','off') 


save(['s2 ankle movement at',num,'0kHz.png'])
filename =sprintf('s2 ankle movement at 90kHz');
print(gcf,filename,'-depsc')
end
function deviation = box(movement)
data =[movement(1:30,1),movement(31:end,1)];
deviation(1,1)=std(data(:,1));
deviation(2,1)=std(data(:,2));
figure(4)
boxplot(data,'Labels',{['Supine,std = ' num2str(deviation(1,1))],['Random, std = ' num2str(deviation(2,1))]});
title('Box plot WB random at 100kHz')
savefig('boxplot_freq100_random.fig')
saveas(gcf,'boxplot_freq100_random.png')
filename =sprintf('boxplotrandom100kHz');
print(gcf,filename,'-depsc')
close all
end
function allplot(x,datapoints2,names)
supine = x(1:30,:);
movement = x(31:60,:);
if movement(:,3) ==movement(:,5)
    disp('equal')
end
for i = 1:5
subplot(1,5,i)
%figure(i)
plot(datapoints2(1:30,1),supine(1:end,i),'-ob')                         
hold on
plot(datapoints2(31:end,1),movement(:,i),'-or')
xlabel('Time (s)', 'FontSize',16)
ylabel('Impedance Value (ohms)','FontSize',16)
titlename = names{i};
title(titlename,'FontSize',17.6,'FontWeight','bold')
end
savefig(['freq90_subject2_Leg.fig'])
end