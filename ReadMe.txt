This file provides instructions on how to use the software on matlab that was used to analyse impedance data for motion artefact characterisation.
The data used for analysis can be found in the 'Data' folder, separated per subject. The ReadMe file inside 'Data' folder explains the structure of the data. 
In the 'Graphs' folder, the codes used to process the graphs can be found. The user can find a separate ReadMeGraphs document describing how to use the codes.

1. Matlab scripts in 'Conversion ohms' folder has the codes to convert raw data from sensor to ohms for all frequencies. 

5 movements folder contains the codes to convert following movements: wrist rotation, arm rotation, ankle rotation, leg flexion and extension and roll side to side. 
 
8 movements folder contains the codes to convert following movements: wrist rotation, wrist up down, arm rotation, arm up down, ankle rotation, ankle up down, leg flexion and extension, roll side to side. Only the data from subject 1 can be converted with the codes from 8 movement folder, since subject 1 raw mat files contain the typo 'wrtistupdown...' instead of 'wristupdown...'. The name of each code file specifies the frequency that is converted, and it converts all segments from that frequency.

   The user has to load the calibration matrices AVG_Freq_10K_100K_30values.mat and ImpedanceSequence.mat to perform the conversion found in the 'Conversion Ohms' folder. The user also needs to upload the mat files with the raw measurements for all segments from the frequency he wants to convert, located in the following folders: Data > Data1 or Data2 (depending on the subject) > data1_raw_mat > 10-20-mat > 10 > all .mat files inside the folders 'Arm_10', 'Leg_10' and 'WB_10'. 

2. Analysis of mean and standard deviation is done using mean_std.m. The script produces all graphs and tables for analysis. 

   If the user wants to analyse the data from subject 1, he needs to change some part of the code as instructed inside the code. The user has to load the collected data from the following folder: Data > Data1 > data1_ohms_converted > allWB/allArm/allLeg, depending on the segment to be analyzed.  

   If the user wants to analyse the data from subject 2, he needs to change some part of the code as instructed inside the code. The user has to load the collected data from the following folder: Data > Data2 > data2_ohms_converted > allvariables.  

3. The artefact identification is done using the identification.m script.

   If the user wants to analyse the data from subject 1, he needs to change some part of the code as instructed inside the code. The user has to load the collected data from the following folder: Data > Data1 > data1_ohms_converted >allWB/allArm/allLeg, depending on the segment to be analyzed.  

   If the user wants to analyse the data from subject 2, he needs to change some part of the code as instructed inside the code. The user has to load the collected data from the following folder: Data > Data2 > data2_ohms_Converted > allvariables.  

4. Analysis of mean and standard deviation for 8 MOVEMENTS, subject 1 ONLY is done using mean_std_8movements_subject1.m. The script produces all graphs and tables for analysis. 
  The user has to load the collected data from the following folder: Data > Data1 > data1_ohms_converted > allWB/allArm/allLeg, depending on the segment to be analyzed.  
